package com.greenwaybicycles.entity;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class OrderDetail {
    private String productName;
    private float subtotal;
    private float total;
 
    public OrderDetail(String productName, float subtotal, float total) {
        this.productName = productName;
        this.subtotal = subtotal;
        this.total = total;
    }
 
    public String getProductName() {
        return productName;
    }
 
    public String getSubtotal() {
        return String.format("%.2f", subtotal);
    }
 
    public String getTotal() {
        return String.format("%.2f", total);
    }
}
