package com.greenwaybicycles.entity;

import java.util.logging.Level;
import java.util.logging.Logger;

public class User {

    private String firstName, surname, password, email, phone, address, role, securityQuestion, securityAnswer;
    public static final String ROLE_CUSTOMER = "CUSTOMER", ROLE_STAFF = "STAFF";
    private Logger logger = Logger.getLogger(User.class.getName());

    /**
     * defaults role to customer
     * 
     * @param aFirstName
     * @param asurname
     * @param aPassword
     * @param aEmail
     * @param aPhone
     * @param aAddress
     */
    public User(String aFirstName, String asurname, String aPassword, String aEmail, String aPhone, String aAddress) {
        firstName = aFirstName;
        surname = asurname;
        setPassword(aPassword);
        email = aEmail;
        phone = aPhone;
        address = aAddress;
        role = ROLE_CUSTOMER;
    }
    
    

    public User(String aFirstName, String aSurname, String aPassword, String aEmail, String aPhone, String aAddress,
            String aRole, String aSecurityQuestion, String aSecurityAnswer) {
        super();
        firstName = aFirstName;
        surname = aSurname;
        setPassword(aPassword);
        email = aEmail;
        phone = aPhone;
        address = aAddress;
        role = aRole;
        securityQuestion = aSecurityQuestion;
        securityAnswer = aSecurityAnswer;
    }



    /**
     * Role must be from User.Role_
     * 
     * @param aFirstName
     * @param asurname
     * @param aPassword
     * @param aEmail
     * @param aPhone
     * @param aAddress
     * @param aRole
     */
    public User(String aFirstName, String asurname, String aPassword, String aEmail, String aPhone, String aAddress,
            String aRole) {
        firstName = aFirstName;
        surname = asurname;
        setPassword(aPassword);
        email = aEmail;
        phone = aPhone;
        address = aAddress;
        setRole(aRole);

    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String aSecurityAnswer) {
        securityAnswer = aSecurityAnswer;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String aSecurityQuestion) {
        securityQuestion = aSecurityQuestion;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String aSurname) {
        surname = aSurname;
    }

    public String getRole() {
        return role;
    }

    /**
     * aRole must be static value from User, User.Role_
     * 
     * @param aRole
     */
    public void setRole(String aRole) {
        if (aRole.equals(ROLE_CUSTOMER) || aRole.equals(ROLE_STAFF)) {
            role = aRole;
        } else {
            logger.log(Level.SEVERE, "Cannot create user with role: " + aRole + ", defaulting to customer");
            if (role == null)
                role = ROLE_CUSTOMER;
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String aEmail) {
        email = aEmail;
    }

    /**
     * returns a hashed password
     * 
     * @return
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String aPassword) {
        // validate that the password use is a hashed password
        if (aPassword.startsWith("$2a$"))
            password = aPassword;
        else
            logger.log(Level.SEVERE, "Password isn't a hashed version");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String aFirstName) {
        firstName = aFirstName;
    }

    /**
     * returns new instance of User with same fields
     */
    public User clone() {
        return new User(firstName, surname, password, email, phone, address, role);
    }

    @Override
    public String toString() {
        return "User [firstName=" + firstName + ", surname=" + surname + ", email=" + email + ", phone=" + phone
                + ", address=" + address + ", role=" + role + "]";
    }

}
