
package com.greenwaybicycles.entity;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Bicycle implements Comparable<Bicycle> {

    private String size;
    private double pricePerDay;
    private String model;
    public static final String SIZE_EXTRA_LARGE = "Extra-Large";
    public static final String SIZE_LARGE = "Large";
    public static final String SIZE_MEDIUM = "Medium";
    public static final String SIZE_SMALL = "Small";
    public static final String MODEL_GIANT = "Giant";
    public int sortingValue;
    private static Logger logger = Logger.getLogger(Bicycle.class.getName());

    public Bicycle(String aSize, double aPricePerDay, String aModel) {
        model = aModel;
        size = aSize;
        pricePerDay = aPricePerDay;
        switch (aSize) {
        case SIZE_EXTRA_LARGE:
            sortingValue = 3;
            break;
        case SIZE_LARGE:
            sortingValue = 2;
            break;
        case SIZE_MEDIUM:
            sortingValue = 1;
            break;
        case SIZE_SMALL:
            sortingValue = 0;
            break;
        default:
            logger.log(Level.WARNING, "Invalid bike size: "+aSize);
            aSize = SIZE_SMALL;
            sortingValue = 0;
            break;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String aModel) {
        model = aModel;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String aSize) {
        size = aSize;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(double aPricePerDay) {
        pricePerDay = aPricePerDay;
    }

    public int getSortingValue() {
        return sortingValue;
    }

    public void setSortingValue(int aSorting_value) {
        sortingValue = aSorting_value;
    }
    
    

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Bicycle(size, pricePerDay, model);
        
    }

    @Override
    public boolean equals(Object aObj) {
        if (aObj == this)
            return true;
        Bicycle bicycle = (Bicycle) aObj;
        boolean result = bicycle.getSize().equals(size) && bicycle.getModel().equals(model);
        return result;
    }

    @Override
    public String toString() {
        return "Bicycle [model=" + size + ", pricePerDay=" + pricePerDay + "]";
    }

    @Override
    public int compareTo(Bicycle bicycle) {
        if (bicycle.getSortingValue() == sortingValue) {
            return 0;
        } else if (bicycle.getSortingValue() > sortingValue) {
            return -1;
        } else {
            return 1;
        }

    }

}
