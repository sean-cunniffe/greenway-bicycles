package com.greenwaybicycles.entity;

import java.time.LocalDate;
import java.util.ArrayList;

public class Booking {

    private User user;

    private boolean pickedUp;

    private LocalDate startDate;

    private LocalDate endDate;

    private ArrayList<Bicycle> bicycles;

    private String pickupLocation;

    private String dropOffLocation;

    public Booking() {
        bicycles = new ArrayList<Bicycle>();
    }

    public Booking(User aUser, LocalDate aStartDate, LocalDate aEndDate, String aPickupLocation,
            String aDropOffLocation) {
        user = aUser;
        startDate = aStartDate;
        endDate = aEndDate;
        pickupLocation = aPickupLocation;
        dropOffLocation = aDropOffLocation;
        bicycles = new ArrayList<Bicycle>();
    }

    public boolean isPickedUp() {
        return pickedUp;
    }

    public void setPickedUp(boolean aPickedUp) {
        pickedUp = aPickedUp;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String aPickupLocation) {
        pickupLocation = aPickupLocation;
    }

    public String getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(String aDropOffLocation) {
        dropOffLocation = aDropOffLocation;
    }

    public ArrayList<Bicycle> getBicycles() {
        return bicycles;
    }

    public void setBicycles(ArrayList<Bicycle> aBicycles) {
        bicycles = aBicycles;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User aUser) {
        user = aUser;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate aStartDate) {
        startDate = aStartDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate aEndDate) {
        endDate = aEndDate;
    }

    @Override
    public String toString() {
        return "Booking [user=" + user + ", pickedUp=" + pickedUp + ", startDate=" + startDate + ", endDate=" + endDate
                + ", bicycles=" + bicycles + ", pickupLocation=" + pickupLocation + ", dropOffLocation="
                + dropOffLocation + "]";
    }

}
