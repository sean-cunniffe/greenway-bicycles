package com.greenwaybicycles.payment;
import java.util.*;

import com.greenwaybicycles.entity.OrderDetail;
import com.paypal.api.payments.*;
import com.paypal.base.rest.*;
 
public class PaymentServices {
    //private static final String CLIENT_ID = "AZXLyBOPBSp6JOf-oBtEbDQaoVxQh9y9lWVuNRqGKsDOuo-sGVuA2OZklq_-KJpnbj8PWriI4l407AyQ";
    //private static final String CLIENT_SECRET = "EIFZI88Dqfbun8-dY5BMR7vJB4crkZ9BwMk0bkV7ZJx6gcYqqMNjiE1w2pqPeGa5rCIyaNk-XRk3FR82";
    
   //sb-vayyt7074658@business.example.com
    private static final String CLIENT_ID = "AVnoZD69KZyOpsKM3YpS16LNGPfjGXOMwsPoR0XbVcXe1n82QWKCv9bEP1lnPYUviaZnGEv63nL7evXa";
    private static final String CLIENT_SECRET = "EIWnl7lVIeNfMYuHfjVfWCU-9PWCon7MzS0jbC52D-yVBfCB7dq79h7Rp8aN55MvCj1CarE20MNFQ-oz";
    
    private static final String MODE = "sandbox";
 
    Payment payment;
    
    public String authorizePayment(OrderDetail orderDetail)        
            throws PayPalRESTException {       
 
        Payer payer = getPayerInformation();
        RedirectUrls redirectUrls = getRedirectURLs();
        List<Transaction> listTransaction = getTransactionInformation(orderDetail);
         
        Payment requestPayment = new Payment();
        requestPayment.setTransactions(listTransaction);
        requestPayment.setRedirectUrls(redirectUrls);
        requestPayment.setPayer(payer);
        requestPayment.setIntent("authorize");
 
        APIContext apiContext = new APIContext(CLIENT_ID, CLIENT_SECRET, MODE);
 
        payment = requestPayment.create(apiContext);
        return getApprovalLink(payment);
 
    }
     
    private Payer getPayerInformation() {
        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");
         
        PayerInfo payerInfo = new PayerInfo();
        payerInfo.setFirstName("Smitha")
                 .setLastName("G")
                 .setEmail("sb-nunzy6945327@personal.example.com");
         
        payer.setPayerInfo(payerInfo);
         
        return payer;
    }
     
    private RedirectUrls getRedirectURLs() {
        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("http://localhost:8080/greenway-bicycles/customer/cancel.xhtml");
        redirectUrls.setReturnUrl("http://localhost:8080/greenway-bicycles/customer/confirm.xhtml");  
        return redirectUrls;
    }
    
    
     
    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment aPayment) {
        payment = aPayment;
    }

    private List<Transaction> getTransactionInformation(OrderDetail orderDetail) {
        Details details = new Details();
        //details.setShipping(orderDetail.getShipping());
        details.setSubtotal(orderDetail.getSubtotal());
        //details.setTax(orderDetail.getTax());
     
        Amount amount = new Amount();
        amount.setCurrency("EUR");
        amount.setTotal(orderDetail.getTotal());
        amount.setDetails(details);
     
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setDescription(orderDetail.getProductName());
         
        ItemList itemList = new ItemList();
        List<Item> items = new ArrayList<Item>();
         
        Item item = new Item();
        item.setCurrency("EUR");
        item.setName(orderDetail.getProductName());
        item.setPrice(orderDetail.getSubtotal());
        //item.setTax(orderDetail.getTax());
        item.setQuantity("1");
         
        items.add(item);
        itemList.setItems(items);
        transaction.setItemList(itemList);
     
        List<Transaction> listTransaction = new ArrayList<Transaction>();
        listTransaction.add(transaction);  
         
        return listTransaction;
    }
     
    private String getApprovalLink(Payment approvedPayment) {
        List<Links> links = approvedPayment.getLinks();
        String approvalLink = null;
         
        for (Links link : links) {
            if (link.getRel().equalsIgnoreCase("approval_url")) {
                approvalLink = link.getHref();
                break;
            }
        }      
         
        return approvalLink;
    }
    public Payment getPaymentDetails(String paymentId) throws PayPalRESTException {
        APIContext apiContext = new APIContext(CLIENT_ID, CLIENT_SECRET, MODE);
        return Payment.get(apiContext, paymentId);
    }
    public Payment executePayment(String paymentId, String payerId)
            throws PayPalRESTException {
        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(payerId);
     
        Payment payment = new Payment().setId(paymentId);
     
        APIContext apiContext = new APIContext(CLIENT_ID, CLIENT_SECRET, MODE);
     
        return payment.execute(apiContext, paymentExecution);
    }
}
