package com.greenwaybicycles.beans;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.greenwaybicycles.dao.BicycleStockManagementDao;
import com.greenwaybicycles.dao.BookingDao;
import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;

@ViewScoped
@ManagedBean
public class BookingBean {
    
    @ManagedProperty(value="#{cartBean}")
    CartBean cartBean;

    @ManagedProperty(value = "#{bookingDao}")
    private BookingDao bookingDao;

    @ManagedProperty(value = "#{bicycleStockManagementDao}")
    private BicycleStockManagementDao bicycleStockManagementDao;

    @ManagedProperty(value = "#{securityBean}")
    private SecurityBean securityBean;

    private TreeMap<Bicycle, Integer> bicycleCountMap;
    private Booking booking;

    @PostConstruct
    public void init() {
        System.out.println("init");
        booking = new Booking();
        booking.setUser(securityBean.getUser());
        booking.setStartDate(LocalDate.now().plusDays(1));
        booking.setEndDate(LocalDate.now().plusDays(2));
        booking.setPickupLocation(bicycleStockManagementDao.getLocations().get(0));
        booking.setDropOffLocation(bicycleStockManagementDao.getLocations().get(0));
        bicycleCountMap = new TreeMap<Bicycle, Integer>();
        for (Bicycle bicycle : bicycleStockManagementDao.getUniqueBicycles()) {
            bicycleCountMap.put(bicycle, 0);
        }
        
        System.out.println(booking.getEndDate().toString());
        System.out.println(booking.getStartDate().toString());
    }
    
    public LocalDate getTodaysDatePlusDays(int days) {
        return LocalDate.now().plusDays(days);
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking aBooking) {
        booking = aBooking;
    }

    public BicycleStockManagementDao getBicycleStockManagementDao() {
        return bicycleStockManagementDao;
    }

    public void updateBasketCount(Bicycle bicycle, String value) {
        int val = Integer.valueOf(value);
        bicycleCountMap.put(bicycle, val);
    }

    public void setBicycleStockManagementDao(BicycleStockManagementDao aBicycleStockManagementDao) {
        bicycleStockManagementDao = aBicycleStockManagementDao;
    }

    public synchronized ArrayList<Entry<Bicycle, Integer>> getAvailableStock() {

        Map<Bicycle, Integer> counts = new TreeMap<Bicycle, Integer>();
        ArrayList<Bicycle> stock = bicycleStockManagementDao.getAllBicycles();
        ArrayList<Booking> bookings = bookingDao.getAllBookings();

        for (Booking tempBooking : bookings) {
            stock.addAll(tempBooking.getBicycles());
        }

        for (Bicycle uniqueBicycle : bicycleStockManagementDao.getUniqueBicycles()) {
            counts.put(uniqueBicycle, 0);
        }
        for (Bicycle bicycle : stock) {
            Integer count = counts.get(bicycle);
            counts.put(bicycle, ++count);
        }

        // minus stock already booked
        for (Booking tempBooking : bookings) {
            boolean isTempBookingStartBeforeBookingEnd = tempBooking.getStartDate().isBefore(booking.getEndDate());
            boolean isTempBookingStartEqualBookingEnd = tempBooking.getStartDate().equals(booking.getEndDate());
            boolean isTempBookingEndAfterBookingStart = tempBooking.getEndDate().isAfter(booking.getStartDate());
            boolean isTempBookingEndEqualBookingStart = tempBooking.getEndDate().equals(booking.getStartDate());

            // if other bookings start date is before our selected end date OR other
            // bookings end date is after our start date --> minus from available bicycles
            if ((isTempBookingStartBeforeBookingEnd || isTempBookingStartEqualBookingEnd)
                    && (isTempBookingEndAfterBookingStart || isTempBookingEndEqualBookingStart)) {
                for (Bicycle bicycle : tempBooking.getBicycles()) {
                    int count = counts.get(bicycle);
                    counts.put(bicycle, --count);
                }
            }
        }

        ArrayList<Map.Entry<Bicycle, Integer>> list = new ArrayList<Map.Entry<Bicycle, Integer>>(counts.entrySet());
        return list;
    }

    public String makeBooking() {
        booking.getBicycles().clear();
        ArrayList<Bicycle> bicycles = bicycleStockManagementDao.getAllBicycles();

        for (Entry<Bicycle, Integer> entry : bicycleCountMap.entrySet()) {
            int count = entry.getValue();
            if (count <= 0) {
                continue;
            }
            for (Bicycle bicycle : bicycles) {
                if (bicycle.equals(entry.getKey())) {
                    booking.getBicycles().add(bicycle);
                    count--;
                    if(count <=0) {
                        break;
                    }
                }
            }
        }
        cartBean.setBooking(booking);
        cartBean.setBicycleCountMap(bicycleCountMap);
        return "confirm-booking.xhtml";
    }

    public TreeMap<Bicycle, Integer> getBicycleCountMap() {
        return bicycleCountMap;
    }

    public void setBicycleCountMap(TreeMap<Bicycle, Integer> aBicycleCountMap) {
        bicycleCountMap = aBicycleCountMap;
    }
    
    

    public CartBean getCartBean() {
        return cartBean;
    }

    public void setCartBean(CartBean aCartBean) {
        cartBean = aCartBean;
    }

    public BookingDao getBookingDao() {
        return bookingDao;
    }

    public void setBookingDao(BookingDao aBookingDao) {
        bookingDao = aBookingDao;
    }

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean aSecurityBean) {
        securityBean = aSecurityBean;
    }

}
