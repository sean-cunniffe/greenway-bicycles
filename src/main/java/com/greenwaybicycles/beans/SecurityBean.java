package com.greenwaybicycles.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCrypt.Hasher;
import at.favre.lib.crypto.bcrypt.BCrypt.Verifyer;
import at.favre.lib.crypto.bcrypt.BCrypt.Version;

/**
 * Manages user authentication on site. Stores the currently logged in user.
 * There are methods to check the role of the user which should be called by
 * each beans that want to check the users role upon loading page in the post
 * construct
 * 
 * @author seanc
 *
 */
@ManagedBean
@SessionScoped
public class SecurityBean implements Serializable {

    private User user;
    public static final Hasher HASHER = BCrypt.with(Version.VERSION_2A);
    public static final Verifyer VERIFIER = BCrypt.verifyer(Version.VERSION_2A);
    public static final String UNAUTHORIZED_NAV = "unauthorized.xhtml";
    public static final String CHANGE_PASSWORD = "change-password.xhtml";
    public static final String MANAGE_ACCOUNT = "manage-account";
    private String securityQuestion;
    private boolean changePassword;

    @ManagedProperty(value = "#{userDao}")
    UserDAO userDao;

    /**
     * if !allowedRoles.contains(user.role) redirect to unauthorized if user is null
     * redirect to login screen
     * 
     * @param allowedRoles
     */
    public boolean checkRole(String... allowedRoles) {
        if (user == null) {
            System.out.println("not logged in");
            redirect(LoginBean.LOGIN_NAV + ".xhtml");
        } else {
            String userRole = user.getRole();
            for (String allowedRole : allowedRoles) {
                if (userRole.equals(allowedRole)) {
                    return true;
                }
            }

        }

        return false;
    }

    public String logout() {
        Map<String, Object> map = getSessionBeans();
        System.out.println("map: " + map.toString());
        for (String key : map.keySet()) {
            boolean killBeans = key.equals("bookingBean") | key.equals("BookingManagementBean") | key.equals("bsmb")
                    | key.equals("BookingManagementBean") | key.equals("cartBean") | key.equals("dashboardBean")
                    | key.equals("manageAccountBean") | key.equals("userManagementBean");
            if (killBeans)
                map.put(key, null);

        }
        user = null;
        return "login.xhtml";
    }

    public boolean redirect(String link) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().dispatch(link);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public void getSecurityQuestionFromEmail(String email) {
        User user = userDao.getUserByEmail(email);
        if (user == null)
            return;
        securityQuestion = user.getSecurityQuestion();
    }

    public void checkQuestion(String answer, String email) {
        User user = userDao.getUserByEmail(email);
        if (answer.equals(user.getSecurityAnswer())) {
            this.user = user;
            setChangePassword(true);
            redirect(CHANGE_PASSWORD);
        } else {
            showMessage("incorrect-answer", "Incorrect answer", FacesMessage.SEVERITY_ERROR, "Incorrect answer");
        }

    }
    
    
    public Map<String, Object> getSessionBeans() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    public void showMessage(String clientId, String summary, Severity level, String details) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(level, summary, details));
    }

    public String verifyPassword(String password) {
        if (verifyUserCred(user.getEmail(), password)) {
            setChangePassword(true);
            return CHANGE_PASSWORD;
        } else {
            showMessage(null, "Incorrect password", FacesMessage.SEVERITY_ERROR, "Incorrect password");
            setChangePassword(false);
            return MANAGE_ACCOUNT;
        }
    }

    public void changePassword(String password) {
        if (isChangePassword()) {
            user.setPassword(hashPassword(password));
            redirect(LoginBean.LOGIN_NAV + ".xhtml");
            setChangePassword(false);
        } else {
            redirect(UNAUTHORIZED_NAV);
        }

    }

    private String hashPassword(String aPassword) {
        return SecurityBean.HASHER.hashToString(4, aPassword.toCharArray());
    }

    public boolean isChangePassword() {
        return changePassword;
    }

    public void setChangePassword(boolean aChangePassword) {
        changePassword = aChangePassword;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String aSecurityQuestion) {
        securityQuestion = aSecurityQuestion;
    }

    public UserDAO getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDAO aUserDao) {
        userDao = aUserDao;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User aUser) {
        user = aUser;
    }

    public boolean verifyUserCred(String aEmail, String aPassword) {
        User user = userDao.getUserByEmail(aEmail);
        if (user != null) {
            boolean isAuth = VERIFIER.verify(aPassword.toCharArray(), user.getPassword()).verified;
            if (isAuth)
                this.user = user;
            return isAuth;
        } else {
            return false;
        }
    }

}
