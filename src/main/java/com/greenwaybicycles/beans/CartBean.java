package com.greenwaybicycles.beans;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.greenwaybicycles.dao.BookingDao;
import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;
import com.greenwaybicycles.entity.OrderDetail;
import com.greenwaybicycles.payment.AuthorizePayment;
import com.greenwaybicycles.payment.PaymentServices;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

@ManagedBean(name = "cartBean")
@SessionScoped
public class CartBean {
    
    Booking booking;
    private TreeMap<Bicycle, Integer> bicycleCountMap;

    private String product;
    private float subTotal, total;
    private Payment payment;
    @ManagedProperty(value = "#{securityBean}")
    private SecurityBean securityBean;

//    @ManagedProperty(value = "#{bookingBean}")
//    private BookingBean bookingBean;

    @ManagedProperty(value = "#{bookingDao}")
    private BookingDao bookingDao;

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean securityBean) {
        this.securityBean = securityBean;
    }

//    public BookingBean getBookingBean() {
//        return bookingBean;
//    }

    
    
    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking aBooking) {
        booking = aBooking;
    }

//    public void setBookingBean(BookingBean bookingBean) {
//        this.bookingBean = bookingBean;
//    }
    
    

    public TreeMap<Bicycle, Integer> getBicycleCountMap() {
        return bicycleCountMap;
    }

    public void setBicycleCountMap(TreeMap<Bicycle, Integer> aBicycleCountMap) {
        bicycleCountMap = aBicycleCountMap;
    }

    public double calculatePayment() {
        System.out.println("Booking:" + booking);
        System.out.println("No of BiCycles:" + booking.getBicycles().size());

        ArrayList<Bicycle> bookedCycles = booking.getBicycles();
        long days = ChronoUnit.DAYS.between(booking.getStartDate(), booking.getEndDate());
        float sum = 0;
        for (Bicycle cycle : bookedCycles) {
            sum += (cycle.getPricePerDay() * days);
        }
        // System.out.println(FacesContext.getCurrentInstance().getViewRoot().getViewId());
        System.out.println("Total payment:" + sum);
        /*
         * FacesContext context = FacesContext.getCurrentInstance(); try {
         * context.getExternalContext().redirect("checkout.xhtml"); } catch (IOException
         * e) { // TODO Auto-generated catch block e.printStackTrace(); }
         */
        product = "greenway-bicycles";
        subTotal = sum;
        total = sum;
        // bookingBean.setBooking(null);
        // bookingBean.setBicycleCountMap(null);
        // booking.setBicycles(null);

        return total;
    }

    public String checkOut() throws IOException {

        try {
            OrderDetail orderDetail = new OrderDetail(getProduct(), getSubTotal(), getTotal());
            PaymentServices paymentServices = new PaymentServices();
            String approvalLink = paymentServices.authorizePayment(orderDetail);
            payment = paymentServices.getPayment();
            System.out.println(payment.getState());
            FacesContext.getCurrentInstance().getExternalContext().redirect(approvalLink);
        } catch (PayPalRESTException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String getApproved() {
        bookingDao.addBooking(booking);
        // booking made, clear bean so init method is called again 
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("bookingBean", null);
        return "approved";

    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String aProduct) {
        product = aProduct;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public BookingDao getBookingDao() {
        return bookingDao;
    }
    
    

    public void setBookingDao(BookingDao aBookingDao) {
        bookingDao = aBookingDao;
    }

    public void setSubTotal(float aSubTotal) {
        subTotal = aSubTotal;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float aTotal) {
        total = aTotal;
    }

}
