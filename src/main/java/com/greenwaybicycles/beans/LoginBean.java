package com.greenwaybicycles.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String email;
    private String password;
    private boolean rememberLogin = true;
    public final static String DASHBOARD_NAV = "dashboard";
    public final static String LOGIN_NAV = "login";

    @ManagedProperty(value = "#{securityBean}")
    private SecurityBean securityBean;

    public LoginBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String aUsername) {
        email = aUsername;
    }

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean aSecurityBean) {
        securityBean = aSecurityBean;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String aPassword) {
        password = aPassword;
    }

    public String validateUserLogin() {
        if (securityBean.verifyUserCred(email, password)) {
            if (!rememberLogin) {
                email = null;
                password = null;
            }
            return DASHBOARD_NAV;
        } else {
            email = null;
            password = null;
            rememberLogin = false;
            FacesContext context = FacesContext.getCurrentInstance();
            if (context != null) {
                FacesContext.getCurrentInstance().addMessage("login-message",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Username/Password Combination",
                                "Invalid Username/Password Combination"));
            }
            // return (null);
            return LOGIN_NAV;
        }
    }

    public boolean isRememberLogin() {
        return rememberLogin;
    }
    
    public void updateAccount() {
        FacesContext.getCurrentInstance().addMessage("successMessage",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Information updated",
                        "Information Updated"));
    }

    public void setRememberLogin(boolean aRememberLogin) {
        rememberLogin = aRememberLogin;
    }

}