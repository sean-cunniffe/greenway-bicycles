package com.greenwaybicycles.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;

import com.greenwaybicycles.dao.BicycleStockManagementDao;
import com.greenwaybicycles.dao.BookingDao;
import com.greenwaybicycles.entity.Booking;

@ManagedBean
@SessionScoped
public class BookingManagementBean implements Serializable {

    public static final String BOOKING_NAV = "booking-management";
    
    private Booking booking;

    private String bookingLocation;

    @ManagedProperty(value = "#{bookingDao}")
    private BookingDao bookingDao;

    @ManagedProperty(value = "#{bicycleStockManagementDao}")
    BicycleStockManagementDao bicycleStockManagementDao;

    @PostConstruct
    public void init() {
        bookingLocation = bookingLocation == null ? bicycleStockManagementDao.getLocations().get(0) : bookingLocation;
    }

    public String getBookingLocation() {
        return bookingLocation;
    }

    public void setBookingLocation(String aBookingLocation) {
        bookingLocation = aBookingLocation;
    }

    public void delete(Booking booking) {
        bookingDao.removeBooking(booking);
    }
    
    public String checkIn(Booking aBooking) {
        aBooking.setPickedUp(true);
        return BOOKING_NAV;
    }
    

    public Booking getBooking() {
        System.out.println("get booking: "+booking);
        return booking;
    }

    public void setBooking(Booking booking) {
        System.out.println("Set booking: "+booking);
        this.booking = booking;
    }

    // Needs to have the naming convention for JSF to find it
    public BookingDao getBookingDao() {
        return bookingDao;
    }
    
    /**
     * sets the booking and shows the dialog to confirm
     * @param booking
     */
    public void showConfirm(Booking booking) {
        setBooking(booking);
        System.out.println("show confirm");
        PrimeFaces.current().executeScript("PF('confirmDialogVar').show();");
    }
    
    /**
     * confirms the booking
     */
    public void confirmBooking() {
        // if bicycles already picked up, then we're returning the bicycles, add them back to the inventory
        // if not picked up, then customer is collecting bicycles
        if(booking.isPickedUp()) {
            bookingDao.removeBooking(booking);
            bicycleStockManagementDao.addBicycles(booking.getBicycles(), booking.getDropOffLocation()); 
        }else {
            booking.setPickedUp(true);
            bicycleStockManagementDao.removeBicycles(booking.getPickupLocation(), booking.getBicycles());
        }
    }

    public void setBookingDao(BookingDao aBookingDao) {
        bookingDao = aBookingDao;
    }

    public BicycleStockManagementDao getBicycleStockManagementDao() {
        return bicycleStockManagementDao;
    }

    public void setBicycleStockManagementDao(BicycleStockManagementDao aBicycleStockManagementDao) {
        bicycleStockManagementDao = aBicycleStockManagementDao;
    }

}