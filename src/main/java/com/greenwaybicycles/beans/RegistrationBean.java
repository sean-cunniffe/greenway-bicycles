package com.greenwaybicycles.beans;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class RegistrationBean {

    @ManagedProperty(value = "#{userDao}")
    UserDAO userDao;
    private String firstName;
    private String surname;
    private String phoneNumber;
    private String currentAddress;
    private String password;
    private String confirmPassword;
    private String email;
    private Logger logger = Logger.getLogger(RegistrationBean.class.getName());
    private UIComponent myEmail;
    private String securityQuestion;
    private String securityQuestionAnswer;

    @PostConstruct
    public void init() {

    }
    public String addUser() {
        String role = User.ROLE_CUSTOMER;
        if(userDao.isStaffEmail(email)) {
            role = User.ROLE_STAFF;
            if(!userDao.getStaffEmails().remove(email))
                logger.log(Level.WARNING, "Could not remove email "+email+" from the staff email list");
        }
        User user = userDao.addUser(firstName, surname, password, email, phoneNumber, currentAddress, role, securityQuestion, securityQuestionAnswer);
        if (user == null) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage errorMessage = new FacesMessage("Email already in use");
            errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(myEmail.getClientId(context), errorMessage);
            return "registration.xhtml";
        }
        logger.log(Level.FINE, "user: " + firstName + " Registered");
        return "view-registration.xhtml";
    }

    public UserDAO getUserDao() {
        return userDao;
    }
    
    public String getSecurityQuestion() {
        return securityQuestion;
    }
    public void setSecurityQuestion(String aSecurityQuestion) {
        securityQuestion = aSecurityQuestion;
    }
    public String getSecurityQuestionAnswer() {
        return securityQuestionAnswer;
    }
    public void setSecurityQuestionAnswer(String aSecurityQuestionAnswer) {
        securityQuestionAnswer = aSecurityQuestionAnswer;
    }
    public UIComponent getMyEmail() {
        return myEmail;
    }

    public void setMyEmail(UIComponent aMyEmail) {
        myEmail = aMyEmail;
    }

    public void setUserDao(UserDAO userDao) {
        this.userDao = userDao;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String register() {
        return ("login");
    }

    public String registerView() {
        return ("view-registration");
    }

    public String getObscuredPassword() {
        return (firstLetter(password) + "..." + lastLetter(password));
    }

    private String firstLetter(String s) {
        return (s.substring(0, 1));
    }

    private String lastLetter(String s) {
        int length = s.length();
        return (s.substring(length - 1, length));
    }
}
