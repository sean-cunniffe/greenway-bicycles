package com.greenwaybicycles.beans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.greenwaybicycles.entity.User;

@ManagedBean
@RequestScoped
public class DashboardBean {

    @ManagedProperty(value = "#{securityBean}")
    SecurityBean securityBean;
    User user;

    @PostConstruct
    public void init() {
        securityBean.checkRole(User.ROLE_CUSTOMER, User.ROLE_STAFF);
        user = securityBean.getUser();

    }

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean aSecurityBean) {
        securityBean = aSecurityBean;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User aUser) {
        user = aUser;
    }

}
