
package com.greenwaybicycles.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;

@ManagedBean
@SessionScoped
public class UserManagementBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userDao}")
    private UserDAO userDao;

    @ManagedProperty(value="#{securityBean}")
    private SecurityBean securityBean;

    private User edittingUser;
    private User oldUserInfo;

    private ArrayList<String> filters;

    private ArrayList<User> users;
    private Logger logger;
    private String userType = "ALL";
    private ArrayList<String> staffEmails;
    public final static String USER_MANAGEMENT_NAV = "user-management";

    public UserManagementBean() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    @PostConstruct
    public void init() {
        securityBean.checkRole(User.ROLE_STAFF);
        
        filters = new ArrayList<String>();
        filters.add("ALL");
        filters.addAll(userDao.getUserTypes());
        users = userDao.getUsers();
        staffEmails = userDao.getStaffEmails();

    }
    
    

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean aSecurityBean) {
        securityBean = aSecurityBean;
    }

    public ArrayList<String> getFilters() {
        return filters;
    }

    public void setFilters(ArrayList<String> aFilters) {
        filters = aFilters;
    }

    public ArrayList<String> getStaffEmails() {
        return staffEmails;
    }

    public void setStaffEmails(ArrayList<String> aStaffEmails) {
        staffEmails = aStaffEmails;
    }

    public User getOldUserInfo() {
        return oldUserInfo;
    }

    public void setOldUserInfo(User aOldUserInfo) {
        oldUserInfo = aOldUserInfo;
    }

    public String getUserType() {
        return userType;
    }

    /**
     * When userType is updated, filter the list for the table
     * 
     * @param aUserType
     */
    public void setUserType(String aUserType) {
        userType = aUserType;
        System.out.println(getUsersForList());
    }

    public void setUsers(ArrayList<User> aUsers) {
        users = aUsers;
    }
    
    

    public ArrayList<User> getUsers() {
        return users;
    }

    public User getEdittingUser() {
        return edittingUser;
    }

    public void setEdittingUser(User aEdittingUser) {
        edittingUser = aEdittingUser;
    }

    public UserDAO getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDAO aUserDao) {
        userDao = aUserDao;
    }

    public String addStaff(String email) {
        // validate input
        if (email == null || email.equals("")) {
            logger.log(Level.WARNING, email + " invalid");
            return USER_MANAGEMENT_NAV;
        }

        if (!userDao.verifyEmailUnique(email) || userDao.isStaffEmail(email)) {
            logger.log(Level.WARNING, email + " exists");
            if(FacesContext.getCurrentInstance() != null)
            FacesContext.getCurrentInstance().addMessage("email-message",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email already in use.", "Email already in use."));
        } else {
            if(FacesContext.getCurrentInstance() != null)
            FacesContext.getCurrentInstance().addMessage("email-message",
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Email added", "Email added."));
            userDao.addStaffEmail(email);
            logger.log(Level.INFO, email + " added");
        }
        return USER_MANAGEMENT_NAV;

    }

    public String removeStaffEmail(String email) {
        if (staffEmails.remove(email))
            FacesContext.getCurrentInstance().addMessage("email-message",
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Email removed", "Email removed."));
        return USER_MANAGEMENT_NAV;
    }

    public String saveUser(boolean save) {
        if (!save) {
            edittingUser.setFirstName(oldUserInfo.getFirstName());
            edittingUser.setSurname(oldUserInfo.getSurname());
            edittingUser.setPhone(oldUserInfo.getPhone());
            edittingUser.setRole(oldUserInfo.getRole());
            edittingUser.setAddress(oldUserInfo.getAddress());
        } else {
            FacesContext.getCurrentInstance().addMessage("user-updated",
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "User Updated", "User Updated"));
        }
        edittingUser = null;
        oldUserInfo = null;
        return USER_MANAGEMENT_NAV;
    }

    public String editUser(User user) {
        if (user != null) {
            System.out.println(user);
            edittingUser = user;
            oldUserInfo = user.clone();
        } else {
            logger.log(Level.WARNING, "Could not edit user: " + user);
        }
        return USER_MANAGEMENT_NAV;
    }

    public String removeUser(User user) {
        userDao.deleteUserByEmail(user.getEmail());
        return USER_MANAGEMENT_NAV;
    }

    public ArrayList<User> getUsersForList() {
        if (userType.equals(User.ROLE_STAFF)) {
            users = userDao.getStaff();
        } else if (userType.equals(User.ROLE_CUSTOMER)) {
            users = userDao.getCustomer();
        } else {
            users = userDao.getUsers();
        }
        return users;
    }

}
