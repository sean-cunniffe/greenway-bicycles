package com.greenwaybicycles.beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.greenwaybicycles.dao.BicycleStockManagementDao;
import com.greenwaybicycles.dao.BookingDao;
import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;

@ManagedBean(name = "bsmb")
@SessionScoped
public class BicycleStockManagementBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(BicycleStockManagementBean.class.getName());
	@ManagedProperty(value = "#{bicycleStockManagementDao}")
	private BicycleStockManagementDao bicycleStockManagementDao;

	@ManagedProperty(value = "#{securityBean}")
	private SecurityBean securityBean;

	private String fromLocation;
	private String toLocation;
	private static final String STOCK_MANAGEMENT_NAV = "bicycleStock";
	private String editStockLocation;

    @ManagedProperty(value = "#{bookingDao}")
    private BookingDao bookingDao;

	private Bicycle editBicycle;


	public BicycleStockManagementBean() {
	}

	@PostConstruct
	public void init() {
//        securityBean.checkRole(User.ROLE_STAFF);

		if (fromLocation == null)
			fromLocation = bicycleStockManagementDao.getLocations().get(0);
		if (toLocation == null)
			toLocation = bicycleStockManagementDao.getLocations().get(1);
		if (editStockLocation == null)
			editStockLocation = fromLocation;

	}
	
	

	public void setToLocation(String aToLocation) {
        toLocation = aToLocation;
    }

    public void setEditStockLocation(String aEditStockLocation) {
        editStockLocation = aEditStockLocation;
    }

    public Bicycle getEditBicycle() {
		return editBicycle;
	}

	public void setEditBicycle(Bicycle aEditBicycle) {
		editBicycle = aEditBicycle;
	}

	public String addBicycle(Bicycle bicycle) {
		if (!bicycleStockManagementDao.addBicycle(editStockLocation, bicycle.getSize(), bicycle.getModel(),
				bicycle.getPricePerDay()))
			logger.log(Level.WARNING, "Could not add bicycle: " + bicycle);
		return STOCK_MANAGEMENT_NAV;
	}

	public String removeBicycle(Bicycle bicycle) {
		if (!bicycleStockManagementDao.removeBicycle(editStockLocation, bicycle))
			logger.log(Level.WARNING, "Could not remove bicycle: " + bicycle);
		return STOCK_MANAGEMENT_NAV;
	}

	public ArrayList<String> locations() {
		return bicycleStockManagementDao.getLocations();
	}

	public ArrayList<Entry<Bicycle, Integer>> getStockCount(String location) {
	    if(location.equals("") | location == null) {
	        return null;
	    }
		System.out.println("Call getStock for location: " + location);
		Map<Bicycle, Integer> counts = new TreeMap<Bicycle, Integer>();
		ArrayList<Bicycle> stock = bicycleStockManagementDao.getBicyclesFromLocation(location);
		/**
		 * adds bicycles that may not be present in location and set count to 0
		 */
		for (Bicycle uniqueBicycle : bicycleStockManagementDao.getUniqueBicycles()) {
			counts.put(uniqueBicycle, 0);
		}

		for (Bicycle bicycle : stock) {
			Integer count = counts.get(bicycle);
			if (count == null) {
				counts.put(bicycle, 1);
			} else {
				counts.put(bicycle, ++count);
			}
		}
		ArrayList<Map.Entry<Bicycle, Integer>> list = new ArrayList<Map.Entry<Bicycle, Integer>>(counts.entrySet());
		return list;
	}


    /**
     * Gets a list of bicycles and the count of how many is needed for the following
     * day
     * 
     * @param location
     * @return
     */
    public ArrayList<Entry<Bicycle, Integer>> getBicyclesNeededForBookings(String location) {
        TreeMap<Bicycle, Integer> counts = new TreeMap<Bicycle, Integer>();
        ArrayList<Booking> bookings = bookingDao.getBookingsFromLocation(location);

        for (Bicycle bicycle : bicycleStockManagementDao.getUniqueBicycles()) {
            counts.put(bicycle, 0);
        }

        for (Booking booking : bookings) {
            if (booking.getStartDate().equals(LocalDate.now().plusDays(1)) && !booking.isPickedUp()
                    && booking.getPickupLocation().equals(location)) {
                for (Bicycle bicycle : booking.getBicycles()) {
                    int count = counts.get(bicycle);
                    counts.put(bicycle, ++count);
                }
            }
        }

        ArrayList<Entry<Bicycle, Integer>> list = new ArrayList<Entry<Bicycle, Integer>>(counts.entrySet());
        return list;
    }

    public ArrayList<Entry<Bicycle, Integer>> getIncomingStockFromBookings(String location) {
        TreeMap<Bicycle, Integer> incomingStockCounts = new TreeMap<Bicycle, Integer>();
        ArrayList<Booking> bookings = bookingDao.getBookingsFromLocation(location);

        for (Bicycle bicycle : bicycleStockManagementDao.getUniqueBicycles()) {
            incomingStockCounts.put(bicycle, 0);
        }

        for (Booking booking : bookings) {
            // checking if the booking is dropping off bicycles tomorrow
            if (booking.getEndDate().equals(LocalDate.now().plusDays(1)) && booking.isPickedUp()
                    && booking.getDropOffLocation().equals(location)) {
                // loop through the bicycles of that booking and add to the count
                for (Bicycle bicycle : booking.getBicycles()) {
                    int count = incomingStockCounts.get(bicycle);
                    incomingStockCounts.put(bicycle, ++count);
                }
            }
        }

        ArrayList<Entry<Bicycle, Integer>> list = new ArrayList<Entry<Bicycle, Integer>>(
                incomingStockCounts.entrySet());
        return list;
    }

    public ArrayList<Entry<Bicycle, Integer>> getStockBalancesAfterBookings(String location) {

        // get current stock
        ArrayList<Entry<Bicycle, Integer>> currentStock = getStockCount(location);

        // get stock needed for tomorrow
        ArrayList<Entry<Bicycle, Integer>> stockNeededForBookings = getBicyclesNeededForBookings(location);

        // get stock incoming from bookings tomorrow
        // ArrayList<Entry<Bicycle, Integer>> incomingStock = getIncomingStockFromBookings(location);

        // Dont add incoming stock, we dont know when theyre bring the stock in so cant
        // rely on them

        // minus stockNeededForBookings
        for (Entry<Bicycle, Integer> neededStockEntry : stockNeededForBookings) {
            for (Entry<Bicycle, Integer> currentStockEntry : currentStock) {
                if (currentStockEntry.getKey().equals(neededStockEntry.getKey())) {
                    int count = currentStockEntry.getValue();
                    currentStockEntry.setValue(count - neededStockEntry.getValue());
                }
            }
        }
        return currentStock;
    }
    
    public void changeBicyclePrices(Bicycle bicycle) {
        System.out.println("Bicycle with price change" + bicycle);
        // get all bicycles currently on site
        for(Bicycle tempBicycle : bicycleStockManagementDao.getAllBicycles()) {
            if(tempBicycle.equals(bicycle)) {
                tempBicycle.setPricePerDay(bicycle.getPricePerDay());
            }
        }
        
        // get all bicycles current in bookings
        for(Booking booking : bookingDao.getAllBookings()) {
            for(Bicycle tempBicycle : booking.getBicycles())
            if(tempBicycle.equals(bicycle)) {
                tempBicycle.setPricePerDay(bicycle.getPricePerDay());
            }
        }
    }


	public SecurityBean getSecurityBean() {
		return securityBean;
	}

	public void setSecurityBean(SecurityBean aSecurityBean) {
		securityBean = aSecurityBean;
	}

	public String moveStock(Bicycle bicycle) {
		bicycleStockManagementDao.moveBicycle(fromLocation, toLocation, bicycle);
		return STOCK_MANAGEMENT_NAV;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String aFromLocation) {
		System.out.println("Changed from location " + fromLocation + " -> " + aFromLocation);
		fromLocation = aFromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public String getEditStockLocation() {
		return editStockLocation;
	}

	public BicycleStockManagementDao getBicycleStockManagementDao() {
		return bicycleStockManagementDao;
	}

	public void setBicycleStockManagementDao(BicycleStockManagementDao aBicycleStockManagementDao) {
		bicycleStockManagementDao = aBicycleStockManagementDao;
	}

    public BookingDao getBookingDao() {
        return bookingDao;
    }

    public void setBookingDao(BookingDao aBookingDao) {
        bookingDao = aBookingDao;
    }
	
	

}
