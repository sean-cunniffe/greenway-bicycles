package com.greenwaybicycles.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.greenwaybicycles.entity.User;

@ManagedBean(name = "manageAccountBean")
@SessionScoped
public class ManageAccountBean implements Serializable{

    @ManagedProperty(value = "#{securityBean}")
    private SecurityBean securityBean;
    
    private User user;
    
    @PostConstruct
    public void init() {
        user = securityBean.getUser();
    }

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean aSecurityBean) {
        securityBean = aSecurityBean;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User aUser) {
        user = aUser;
    }
    
    
    
}
