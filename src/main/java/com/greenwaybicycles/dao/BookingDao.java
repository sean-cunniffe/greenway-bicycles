package com.greenwaybicycles.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;
import com.greenwaybicycles.entity.User;

//application scope, bean created with server starts
@ManagedBean(name = "bookingDao", eager = true)
@ApplicationScoped
public class BookingDao {

    private ArrayList<Booking> bookings;
    private ArrayList<Booking> oldBookings;
    private Logger logger;

    public BookingDao() {

        logger = Logger.getLogger(this.getClass().getName());
        bookings = new ArrayList<>();
        oldBookings = new ArrayList<Booking>();
        logger.log(Level.INFO, "New Booking Added");
    }

    @PostConstruct
    public void init() {
//        Booking booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
//                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Athlone");
//        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_SMALL, 10, Bicycle.MODEL_GIANT));
//        Booking booking2 = new Booking(new User("Sean", "Cunniffe", "", "john@gmail.com", "0871234567", "someplace"),
//                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Athlone");
//        booking2.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking2.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking2.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking2.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking2.getBicycles().add(new Bicycle(Bicycle.SIZE_SMALL, 10, Bicycle.MODEL_GIANT));
//        Booking booking3 = new Booking(new User("Sean", "Cunniffe", "", "mary@gmail.com", "0871234567", "someplace"),
//                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Athlone");
//        booking3.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking3.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking3.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking3.getBicycles().add(new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT));
//        booking3.getBicycles().add(new Bicycle(Bicycle.SIZE_SMALL, 10, Bicycle.MODEL_GIANT));
//        
//        addBooking(booking);
//        addBooking(booking2);
//        addBooking(booking3);
        
    }
    
    

    public ArrayList<Booking> getAllBookings() {
        return bookings;

    }

    public void addBooking(Booking booking) {
        bookings.add(booking);
    }

//    public void addBooking(User user, LocalDate startDate, LocalDate endDate, String pickup, String dropOff) {
//        Booking booking = new Booking(user, startDate, endDate, pickup, dropOff);
//        bookings.add(booking);
//    }

    public ArrayList<Booking> getBookingsByUsersEmail(String email) {
        ArrayList<Booking> userBookings = new ArrayList<Booking>();
        for (Booking booking : bookings) {
            if (booking.getUser().getEmail().equals(email)) {
                userBookings.add(booking);
            }
        }
        return userBookings;
    }

    /**
     * Returns a list of booking if there dropOff or pickUp is the location
     * 
     * @author seanc
     * @param location
     * @return
     */
    public ArrayList<Booking> getBookingsFromLocation(String location) {
        ArrayList<Booking> tempBookings = new ArrayList<Booking>();
        for (Booking booking : bookings) {
            if (booking.getPickupLocation().equals(location)) {
                tempBookings.add(booking);
            } else if (booking.getDropOffLocation().equals(location)) {
                tempBookings.add(booking);
            }

        }
        return tempBookings;
    }
    /**Return a list of active bookings. An active booking is a booking thats end date is 
     * after todays date
     * @author seanc
     * @param location
     * @return
     */
    public ArrayList<Booking> getActiveBookingsFromLocation(String location) {
        ArrayList<Booking> tempBookings = new ArrayList<Booking>();
        for (Booking booking : bookings) {
            // if enddate is after todays date, then it is no longer active and we just keep it for history
            if(booking.getEndDate().isBefore(LocalDate.now()))
                continue;
            if (booking.getPickupLocation().equals(location) && !booking.isPickedUp()) {
                tempBookings.add(booking);
            } else if (booking.getDropOffLocation().equals(location) && booking.isPickedUp()) {
                tempBookings.add(booking);
            }

        }
        return tempBookings;
    }

    /**
     * Removes booking from booking and adds to old bookings
     * @param booking
     * @return
     */
    public boolean removeBooking(Booking booking) {
        oldBookings.add(booking);
        return bookings.remove(booking);
    }
    public ArrayList<Bicycle> getBicyclesFromBookingsInLocationOnDate(String location, LocalDate date) {
        ArrayList<Booking> tempBookings = new ArrayList<Booking>();
        ArrayList<Bicycle> bikeList = new ArrayList<Bicycle>();
        System.out.println(date);
        System.out.println(location);
       if(date==null) {
    	   return null;
       }
        for (Booking booking : bookings) {
            if (booking.getPickupLocation().equals(location) && booking.getStartDate().equals(date)) {
                tempBookings.add(booking);
            } 
        }
        for (Booking booking : tempBookings) {
        	bikeList.addAll(booking.getBicycles());
        }
        return bikeList;
    }
    public int getBicyclesPickups(String location, LocalDate date) {
        int pickUps = 0;
        System.out.println(date);
        System.out.println(location);
       if(date==null) {
    	   return 0;
       }
        for (Booking booking : bookings) {
            if (booking.getPickupLocation().equals(location) && booking.getStartDate().equals(date)&&booking.isPickedUp()) {
            	pickUps+= booking.getBicycles().size();
            	
            } 
        }
        for (Booking booking : oldBookings) {
            if (booking.getPickupLocation().equals(location) && booking.getStartDate().equals(date)) {
            	pickUps+= booking.getBicycles().size();
            } 
            
        }
        return pickUps;
    }
    public int getBicyclesDropOffs(String location, LocalDate date) {
        int dropOffs = 0;
        System.out.println(date);
        System.out.println(location);
       if(date==null) {
    	   return 0;
       }
        for (Booking booking : oldBookings) {
            if (booking.getDropOffLocation().equals(location) && booking.getEndDate().equals(date)) {
            	dropOffs+= booking.getBicycles().size();
            } 
        }
        return dropOffs;
    }


}
