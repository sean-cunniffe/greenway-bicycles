package com.greenwaybicycles.dao;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.greenwaybicycles.beans.SecurityBean;
import com.greenwaybicycles.entity.User;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCrypt.Hasher;
import at.favre.lib.crypto.bcrypt.BCrypt.Verifyer;
import at.favre.lib.crypto.bcrypt.BCrypt.Version;

//application scope, bean created with server starts
@ManagedBean(name = "userDao", eager = true)
@ApplicationScoped
public class UserDAO {

    private ArrayList<User> users;
    private ArrayList<String> staffEmails;
    private Logger logger;
    private ArrayList<String> userTypes;

    public UserDAO() {
        users = new ArrayList<>();
        staffEmails = new ArrayList<String>();
        logger = Logger.getLogger(this.getClass().getName());
        userTypes = new ArrayList<String>();
        userTypes.add(User.ROLE_CUSTOMER);
        userTypes.add(User.ROLE_STAFF);
    }
    
    @PostConstruct
    public void init() {
        

        logger.log(Level.INFO, "staff added");
        addUser("Sean", "Cunniffe", "Test123", "sean.cunniffe927@gmail.com", "0871234567", "someplace",
                User.ROLE_STAFF, "Mothers name?","Sharan");
        logger.log(Level.INFO, "staff added");
        addUser("Jonathan", "Doe", "Test123", "john.doe@gmail.com", "0879876543", "somewhere", User.ROLE_CUSTOMER);
        addStaffEmail("kevin@gmail.com");
        addUser("Jonathan", "Doe", "Test123", "john.doe@gmail.com", "0879876543", "somewhere", User.ROLE_CUSTOMER);
    }

    /**
     * Creates and adds user to the user arraylist, hashes password using bcrypt
     * returns null if user isnt added
     * 
     * @param aFirstName
     * @param aLastName
     * @param aPassword
     * @param aEmail
     * @return
     */
    public User addUser(String aFirstName, String aLastName, String aPassword, String aEmail, String aPhone,
            String aAddress, String role) {
        User user = null;
        // verify email is unique
        if (verifyEmailUnique(aEmail)) {
            // hash password
            String hashedPassword = hashPassword(aPassword);
            user = new User(aFirstName, aLastName, hashedPassword, aEmail, aPhone, aAddress, role);
            users.add(user);
        }
        return user;
    }

    public User addUser(String aFirstName, String aLastName, String aPassword, String aEmail, String aPhone,
            String aAddress, String role, String securityQuestion, String securityQuestionAnswer) {
        User user = null;
        // verify email is unique
        if (verifyEmailUnique(aEmail)) {
            // hash password
            String hashedPassword = hashPassword(aPassword);
            user = new User(aFirstName, aLastName, hashedPassword, aEmail, aPhone, aAddress, role, securityQuestion,
                    securityQuestionAnswer);
            users.add(user);
        }
        return user;
    }

    /**
     * finds user by email address
     * 
     * @param aEmail
     * @return the user or null if doesnt exist
     */
    public User getUserByEmail(String aEmail) {
        aEmail = aEmail.toLowerCase();
        for (User user : users) {
            if (aEmail.equals(user.getEmail().toLowerCase()))
                return user;

        }
        return null;
    }

    /**
     * updates user when email == aEmail and currentPassword is equal to users
     * password, if a param is null then that field isn't updated
     * 
     * @param aFirstName
     * @param aLastName
     * @param aPassword
     * @param aEmail
     * @return updated user or null if user wasn't found
     * @throws InvalidCredentialsException
     */
    public User updateUserByEmail(String aFirstName, String aLastName, String aPassword, String aEmail) {
        User user = getUserByEmail(aEmail);
        if (aFirstName != null)
            user.setFirstName(aFirstName);
        if (aLastName != null)
            user.setSurname(aLastName);
        if (aPassword != null)
            user.setPassword(hashPassword(aPassword));
        return user;
    }

    /**
     * finds user with the specified email and deletes them from the user arraylist
     * if user with email doesnt exist then return false
     * 
     * @param aEmail
     * @return successfully deleted user
     */
    public boolean deleteUserByEmail(String aEmail) {
        User user = getUserByEmail(aEmail);
        System.out.println("deleing user" + user);
        if (user == null)
            return false;
        return users.remove(user);
    }

    /**
     * returns true if getUserByEmail == null
     * 
     * @param aEmail
     * @return if email doesnt belong to user of system
     */
    public boolean verifyEmailUnique(String aEmail) {
        return getUserByEmail(aEmail) == null;
    }

    /**
     * Hash a password using bcrypt
     * 
     * @param aPassword
     * @return a hashed version of password
     */
    private String hashPassword(String aPassword) {
        return SecurityBean.HASHER.hashToString(4, aPassword.toCharArray());
    }

    /**
     * 
     * @return Arraylist of users
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * Adds email to staff list if the email isnt already in the list and
     * isnt being used by a user
     * @param email
     * @return
     */
    public boolean addStaffEmail(String email) {
        if (verifyEmailUnique(email) && !staffEmails.contains(email)) {
            return staffEmails.add(email);
        } else {
            logger.log(Level.WARNING, email + " is already a staff email");
            return false;
        }
    }

    public boolean isStaffEmail(String email) {
        for (String staffEmail : staffEmails) {
            if (staffEmail.equals(email)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<User> getStaff() {
        ArrayList<User> staff = new ArrayList<>();
        for (User user : users) {
            if (user.getRole().equals(User.ROLE_STAFF)) {
                staff.add(user);
            }
        }
        return staff;
    }

    public ArrayList<User> getCustomer() {
        ArrayList<User> staff = new ArrayList<>();
        for (User user : users) {
            if (user.getRole().equals(User.ROLE_CUSTOMER)) {
                staff.add(user);
            }
        }
        return staff;
    }

    public ArrayList<String> getUserTypes() {
        return userTypes;
    }


    public void setUsers(ArrayList<User> aUsers) {
        users = aUsers;
    }

    public ArrayList<String> getStaffEmails() {
        return staffEmails;
    }

    public void setStaffEmails(ArrayList<String> aStaffEmails) {
        staffEmails = aStaffEmails;
    }
    
    
    
}
