package com.greenwaybicycles.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.greenwaybicycles.entity.Bicycle;

@ManagedBean(name = "bicycleStockManagementDao")
@ApplicationScoped
public class BicycleStockManagementDao {

    private HashMap<String, ArrayList<Bicycle>> locations;
    Logger logger;
    ArrayList<Bicycle> uniqueBicycles;

    public BicycleStockManagementDao() {
        logger = Logger.getLogger(this.getClass().getName());
        uniqueBicycles = new ArrayList<Bicycle>();
    }

    @PostConstruct
    public void init() {
        locations = new HashMap<String, ArrayList<Bicycle>>();
        locations.put("Mullingar", new ArrayList<Bicycle>());
        locations.put("Athlone", new ArrayList<Bicycle>());
        locations.put("Castletown", new ArrayList<Bicycle>());
        
//         adds 5 medium bicycles to each location
            for (int i = 0; i < 10; i++) {
                addBicycle("Mullingar", Bicycle.SIZE_MEDIUM, Bicycle.MODEL_GIANT, 20);
                addBicycle("Athlone", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 20);
                addBicycle("Castletown", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 20);
            }
            for (int i = 0; i < 8; i++) {
                addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 20);
                addBicycle("Athlone", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 20);
                addBicycle("Castletown", Bicycle.SIZE_MEDIUM, Bicycle.MODEL_GIANT, 20);
            }
            for (int i = 0; i < 12; i++) {
                addBicycle("Mullingar", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 20);
                addBicycle("Athlone", Bicycle.SIZE_MEDIUM, Bicycle.MODEL_GIANT, 20);
                addBicycle("Castletown", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 20);
            }
            for (int i = 0; i < 12; i++) {
                addBicycle("Mullingar", Bicycle.SIZE_EXTRA_LARGE, Bicycle.MODEL_GIANT, 20);
                addBicycle("Athlone", Bicycle.SIZE_EXTRA_LARGE, Bicycle.MODEL_GIANT, 20);
                addBicycle("Castletown", Bicycle.SIZE_EXTRA_LARGE, Bicycle.MODEL_GIANT, 20);
            }
    }
    
    public void removeBicycles(String fromLocation, ArrayList<Bicycle> bicycles) {
        ArrayList<Bicycle> bicyclesLocation = getBicyclesFromLocation(fromLocation);
        for(Bicycle bicycle : bicycles) {
            if(!bicyclesLocation.remove(bicycle)) {
                logger.log(Level.SEVERE,"Could not remove bicycle "+bicycle+" from location"+fromLocation);
            }
        }
    }
    
    // TODO uses a table, if a booking has the bicycles and we change the prices, it wont change those bicycle prices
    public void changeBicyclePrices(Bicycle bicycle) {
        System.out.println("Bicycle with price change" + bicycle);
        for(Bicycle tempBicycle : getAllBicycles()) {
            if(tempBicycle.equals(bicycle)) {
                tempBicycle.setPricePerDay(bicycle.getPricePerDay());
                System.out.println(tempBicycle);
            }
        }
    }
    

    public boolean moveBicycle(String fromLocationName, String toLocationName, String size) {
        ArrayList<Bicycle> fromLocation = locations.get(fromLocationName);
        ArrayList<Bicycle> toLocation = locations.get(toLocationName);
        Bicycle bicycle = null;

        for (Bicycle tempBicycle : fromLocation) {
            if (tempBicycle.getSize().equals(size)) {
                bicycle = tempBicycle;
                break;
            }
        }
        if (bicycle == null) {
            logger.log(Level.WARNING, "Could not find bicycle of size " + size + " in " + fromLocationName);
            return false;
        } else {
            fromLocation.remove(bicycle);
            return toLocation.add(bicycle);
        }

    }
    
    public boolean moveBicycle(String fromLocationName, String toLocationName, Bicycle bicycle) {
        ArrayList<Bicycle> fromLocation = locations.get(fromLocationName);
        ArrayList<Bicycle> toLocation = locations.get(toLocationName);
        if(fromLocation.remove(bicycle))
            return toLocation.add(bicycle);
        return false;

    }

    /**
     * returns count of bicycles with changed price
     * 
     * @param locationName
     * @param size
     * @param cost
     * @return
     */

    public boolean addBicycle(String locationName, String size, String model, double cost) {
        Bicycle bicycle = new Bicycle(size, cost, model);
        ArrayList<Bicycle> location = locations.get(locationName);
        // keep track of all unique bicycles being added to the system
        if(!uniqueBicycles.contains(bicycle)) {
            uniqueBicycles.add(bicycle);
        }
        return location.add(bicycle);
    }
    
    public boolean addBicycles(List<Bicycle> biycles, String location) {
        return (getBicyclesFromLocation(location).addAll(biycles));
    }
    
    
    /**
     * returns list of all unique bicycle combinations, save computing everytime
     * @return
     */
    public ArrayList<Bicycle> getUniqueBicycles() {
        return uniqueBicycles;
    }


    /**
     * gets the bicycles in location
     * 
     * @param location
     * @return
     */
    public ArrayList<Bicycle> getBicyclesFromLocation(String location) {
        return locations.get(location);
    }

    /**
     * gets a list of the locations
     * 
     * @return
     */

    public ArrayList<String> getLocations() {
        ArrayList<String> locations = new ArrayList<>();
        for (String locString : this.locations.keySet()) {
            locations.add(locString);
        }
        return locations;
    }

//    /**
//     * Returns bicycle with param size in the location, returns null if no bicycle
//     * was found
//     * 
//     * @param location
//     * @param size
//     * @return
//     */
//    public Bicycle getBicycleOfSize(String location, String size) {
//        ArrayList<Bicycle> bicycles = getBicyclesFromLocation(location);
//        for (Bicycle bicycle : bicycles) {
//            if (bicycle.getSize().equals(size)) {
//                return bicycle;
//            }
//        }
//        return null;
//    }

    public ArrayList<Bicycle> getAllBicycles() {
        ArrayList<Bicycle> bicycles = new ArrayList<Bicycle>();

        for (Entry<String, ArrayList<Bicycle>> locations : locations.entrySet()) {
            ArrayList<Bicycle> locationBicycles = locations.getValue();
            bicycles.addAll(locationBicycles);
        }
        return bicycles;
    }

    public boolean removeBicycle(String aLocation, Bicycle bicycle) {
        ArrayList<Bicycle> bicycles = getBicyclesFromLocation(aLocation);
        return bicycles.remove(bicycle);
        
    }

    public void setLocations(HashMap<String, ArrayList<Bicycle>> aLocations) {
        locations = aLocations;
    }

    public void setUniqueBicycles(ArrayList<Bicycle> aUniqueBicycles) {
        uniqueBicycles = aUniqueBicycles;
    }
    
    
    
}
