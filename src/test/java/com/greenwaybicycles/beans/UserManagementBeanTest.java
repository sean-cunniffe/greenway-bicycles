package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicContainer.*;
import static org.junit.jupiter.api.DynamicTest.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;

class UserManagementBeanTest {
    
    UserManagementBean bean;
    
	@BeforeEach
	void setUp()  {
	    bean = new UserManagementBean();
	    ArrayList<String> filter = new ArrayList<String>();
	    //filter.addAll(List.of(User.ROLE_CUSTOMER, User.ROLE_STAFF, "ALL"));
	    bean.setFilters(filter);
        UserDAO userDao = new UserDAO();
        userDao.init();
        userDao.setUsers(new ArrayList<User>());
        userDao.setStaffEmails(new ArrayList<String>());

        User user = userDao.addUser("John", "Doe", "Test123", "John.doe@gmail.com", "09876546778", "Athlone",
                User.ROLE_STAFF);
        userDao.addStaffEmail("sc@gmail.com");
        bean.setUserDao(userDao);

        SecurityBean securityBean = new SecurityBean();
        securityBean.setUser(user);
        bean.setSecurityBean(securityBean);
        bean.init();
	}
	
	@Test
    void testGetterAndSetters() throws Exception {
	    SecurityBean securityBean = new SecurityBean();
	    bean.setSecurityBean( securityBean);
        assertEquals(securityBean, bean.getSecurityBean());
        
        ArrayList<String> filters = new ArrayList<String>();
        bean.setFilters(filters);
        assertEquals(filters, bean.getFilters());
        assertNotNull(bean.getUserType());
        ArrayList<String> staffEmail = new ArrayList<String>();
        bean.setStaffEmails(staffEmail);
        assertEquals(staffEmail, bean.getStaffEmails());
        bean.setEdittingUser(bean.getUserDao().addUser(null, null, "test", "test", null, null, User.ROLE_CUSTOMER));
        assertNotNull(bean.getEdittingUser());
    }
	
	@Test
    void testSaveUser()  {
	    User user = new User("sean", "doe","","","","", User.ROLE_STAFF);
        bean.setEdittingUser(user);
        User oldUser = new User("kevin", "david","","kevin@gmail.com", "0873422256","someplace");
        bean.setOldUserInfo(oldUser);
        bean.saveUser(false);
        assertEquals(oldUser.getFirstName(), user.getFirstName());
        assertEquals(oldUser.getSurname(), user.getSurname());
        assertEquals(oldUser.getPhone()  , user.getPhone());
        assertEquals(oldUser.getAddress(), user.getAddress());
    }
	
	@Test
    void testAddStaff()  {
	    assertEquals("user-management",  bean.addStaff(null));
	    assertEquals("user-management",  bean.addStaff(""));
	    assertEquals("user-management",  bean.addStaff("sean@gmail.com"));
	    bean.getUserDao().addUser("", "", "test", "sean@gmail.com", "", "", User.ROLE_STAFF);
        assertEquals("user-management",  bean.addStaff("sean@gmail.com"));
        bean.getUserDao().addStaffEmail("sean@gmail.com");
        assertEquals("user-management",  bean.addStaff("sean@gmail.com"));
    }
	
	@Test
    void testRemoveStaffFail()  {
        assertEquals("user-management", bean.removeStaffEmail("sea@gmail.com"));
    }
	
	@Test
    void testEditUser()  {
	    User user = bean.getUserDao().addUser("", "", "test", "sean@gmail.com", "", "", User.ROLE_STAFF);
	    assertEquals( "user-management",bean.editUser(user));
	    assertEquals( "user-management",bean.editUser(null));
    }
	
	@Test
    void testRemoveUser()  {
	    User user = bean.getUserDao().addUser("", "", "test", "sean@gmail.com", "", "", User.ROLE_STAFF);
	    assertEquals("user-management", bean.removeUser(user));
    }
	
	@Test
    void testGetUsersForList()  {
	    bean.setUserType(User.ROLE_STAFF);
	    bean.getUserDao().addUser("", "", "test", "sean@gmail.com", "", "", User.ROLE_STAFF);
	    bean.getUserDao().addUser("", "", "test", "sean2@gmail.com", "", "", User.ROLE_CUSTOMER);
        assertEquals( 2,bean.getUsersForList().size());
        bean.setUserType(User.ROLE_CUSTOMER);
        assertEquals( 1,bean.getUsersForList().size());
        bean.setUserType("ALL");
        assertEquals( 3,bean.getUsersForList().size());
    }

}
