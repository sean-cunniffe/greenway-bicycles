package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;
import com.greenwaybicycles.entity.OrderDetail;
import com.greenwaybicycles.entity.User;
import com.greenwaybicycles.payment.PaymentServices;

class CartBeanTest {
	CartBean cartbean;
	@BeforeEach
	void setUp() throws Exception {
		cartbean=new CartBean();
		
		
	}

	@Test
	void testChangeProductName() {
		cartbean.setProduct("greenway-bycycle");
		assertEquals("greenway-bycycle",cartbean.getProduct());
	}
	@Test
	void testChangeSubTotal() {
		cartbean. setSubTotal(20.0f);
		assertEquals(20.0f,cartbean.getSubTotal());
	}
	@Test
	void testChangeTotal() {
		cartbean. setTotal(20.0f);
		assertEquals(20.0f,cartbean.getTotal());
	}
	@Test
	void testSecurityBean() {
		SecurityBean sb=new SecurityBean();
		cartbean.setSecurityBean(sb); 
		UserDAO userDao = new UserDAO();
        userDao.addUser("john", "doe", "Test123", "john.doe@gmail.com", "0872345678", "someplace", User.ROLE_STAFF);
        sb.setUserDao(userDao);
        assertEquals(sb,cartbean.getSecurityBean());
	}
	
	@Test
	void testcheckOut() {
		 OrderDetail orderDetail = new OrderDetail("greenway-bycycle", 20.0f, 20.0f);
		 PaymentServices paymentServices = new PaymentServices();
		 assertDoesNotThrow(()->{ paymentServices.authorizePayment(orderDetail);});
		

	}
	@Test
	void testcalculatePayment() {
		
		Booking booking = new Booking(new User("smitha", "Gujjaru", "", "smitha@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Athlone", "Mullingar");
		Bicycle bicycleS = new Bicycle(Bicycle.SIZE_SMALL, 20, Bicycle.MODEL_GIANT);
		Bicycle bicycleM = new Bicycle(Bicycle.SIZE_MEDIUM, 20, Bicycle.MODEL_GIANT);
		Bicycle bicycleL = new Bicycle(Bicycle.SIZE_LARGE, 20, Bicycle.MODEL_GIANT);
		Bicycle bicycleXL = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT);
		ArrayList list=new ArrayList();list.add(bicycleS);list.add(bicycleM);list.add(bicycleL);list.add(bicycleXL);
		
		booking.setBicycles(list);
		BookingBean bookingBean=new BookingBean();
		bookingBean.setBooking(booking);
		cartbean.setBooking(booking);
		//cartbean.setBookingBean(bookingBean); 
		assertEquals(80.0,cartbean.calculatePayment());

	}
	
	@Test
	void testcheckOutOrder() {
		cartbean.setProduct("greenway-bycycle");
		cartbean.setSubTotal(1.0f);
		cartbean.setTotal(1.0f);
		Assertions.assertThrows(NullPointerException.class, () -> {cartbean.checkOut();});
	}
}
