package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.dao.UserDAO;

class RegistrationTest {
    RegistrationBean registration;

    @BeforeEach
    void setUp() throws Exception {
        registration = new RegistrationBean();
        UserDAO userDao = new UserDAO();
        registration.setUserDao(userDao);
        userDao.addStaffEmail("john.doe@gmail.com");
    }

    @Test
    void testChangeFirstName() {
        registration.setFirstName("smitha");
        assertEquals("smitha", registration.getFirstName());

    }
    
    @Test
    void testChangeUserDao() {
        UserDAO dao = new UserDAO();
        registration.setUserDao(dao);
        assertEquals(dao,registration.getUserDao());
    }

    @Test
    void testChangeSurname() {
        registration.setSurname("Gujjaru");
        assertEquals("Gujjaru", registration.getSurname());

    }

    @Test
    void testChangePhoneNumber() {
        registration.setPhoneNumber("0897654323");
        assertEquals("0897654323", registration.getPhoneNumber());

    }

    @Test
    void testChangeEmail() {
        registration.setEmail("Smitha@gmail.com");
        assertEquals("Smitha@gmail.com", registration.getEmail());

    }

    @Test
    void testChangePassword() {
        registration.setPassword("Zse@w1");
        assertEquals("Zse@w1", registration.getPassword());

    }

    @Test
    void testChangeConfirmPassword() {
        registration.setConfirmPassword("Zse@w1");
        assertEquals("Zse@w1", registration.getConfirmPassword());

    }

    @Test
    void testChangeCurrentAddress() {
        registration.setCurrentAddress("Athone");
        assertEquals("Athone", registration.getCurrentAddress());

    }

    @Test
    void testRegistrationSuccessful() {
        registration.setFirstName("smitha");
        registration.setSurname("Gujjaru");
        registration.setPhoneNumber("0897654323");
        registration.setEmail("Smitha@gmail.com");
        registration.setCurrentAddress("Athone");
        registration.setPassword("Zse@w1");
        registration.setConfirmPassword("Zse@w1");

        assertEquals("view-registration", registration.registerView());

    }

    @Test
    void testLoginThePage() {
        registration.setFirstName("smitha");
        registration.setSurname("Gujjaru");
        registration.setPhoneNumber("0897654323");
        registration.setEmail("Smitha@gmail.com");
        registration.setCurrentAddress("Athone");
        registration.setPassword("Zse@w1");
        registration.setConfirmPassword("Zse@w1");
        assertEquals("login", registration.register());

    }
    
    @Test
    void testAdduserEmailExists() {
        registration.setFirstName("smitha");
        registration.setSurname("Gujjaru");
        registration.setPhoneNumber("0897654323");
        registration.setEmail("Smitha@gmail.com");
        registration.setCurrentAddress("Athone");
        registration.setPassword("Zse@w1");
        registration.setConfirmPassword("Zse@w1");
        assertEquals("view-registration.xhtml", registration.addUser());
        registration.setFirstName("smithaNew");
        registration.setSurname("GujjaruNew");
        registration.setPhoneNumber("0897654323");
        registration.setEmail("Smitha@gmail.com");
        registration.setCurrentAddress("Athone");
        registration.setPassword("Zse@w1");
        registration.setConfirmPassword("Zse@w1");
        Assertions.assertThrows(NullPointerException.class, () -> {registration.addUser();});

    }

    @Test
    void testgetObscuredPassword() {

        registration.setPassword("Zse@w1");
        // registration.setConfirmPassword("Zse@w1");
        assertEquals("Z...1", registration.getObscuredPassword());

    }

    @Test
    void addCustomer() {
        registration.setFirstName("smitha");
        registration.setSurname("Gujjaru");
        registration.setPhoneNumber("0897654323");
        registration.setEmail("Smitha@gmail.com");
        registration.setCurrentAddress("Athone");
        registration.setPassword("Zse@w1");
        registration.setConfirmPassword("Zse@w1");
        assertEquals("view-registration.xhtml", registration.addUser());
    }
    
    @Test
    void addStaff() {
        registration.setFirstName("smitha");
        registration.setSurname("Gujjaru");
        registration.setPhoneNumber("0897654323");
        registration.setEmail("john.doe@gmail.com");
        registration.setCurrentAddress("Athone");
        registration.setPassword("Zse@w1");
        registration.setConfirmPassword("Zse@w1");
        assertEquals("view-registration.xhtml", registration.addUser());
    }

}
