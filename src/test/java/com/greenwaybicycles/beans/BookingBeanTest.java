package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.dao.BicycleStockManagementDao;
import com.greenwaybicycles.dao.BookingDao;
import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;
import com.greenwaybicycles.entity.User;

class BookingBeanTest {

    BookingBean bookingBean;
    BookingDao bookingDao;
    Booking booking ;
    BicycleStockManagementDao bsm;

    @BeforeEach
    void setUp() throws Exception {
        bookingBean = new BookingBean();
        bookingDao = new BookingDao();
        bookingDao.init();
        bookingBean.setCartBean(new CartBean());
        booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Mullingar");
        bookingDao.addBooking(booking);
        bookingBean.setBookingDao(bookingDao);
        bsm = new BicycleStockManagementDao();
        bsm.init();
        bookingBean.setBicycleStockManagementDao(bsm);

        bookingBean.setSecurityBean(new SecurityBean());
        bookingBean.init();
    }

    @Test
    void testGetTodaysDatePlusDays() {
        assertEquals( LocalDate.now().plusDays(1),bookingBean.getTodaysDatePlusDays(1));
    }

    @Test
    void testGetBooking() {
        bookingBean.setBooking(booking);
        assertEquals(booking, bookingBean.getBooking());
    }

    @Test
    void testGetBicycleStockManagementDao() {
        bookingBean.setBicycleStockManagementDao(bsm);
        assertEquals(bsm, bookingBean.getBicycleStockManagementDao());
    }

    @Test
    void testUpdateBasketCount() {
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 30, Bicycle.MODEL_GIANT);
        bookingBean.updateBasketCount( bicycle, "3");
        assertEquals( 3, bookingBean.getBicycleCountMap().get(bicycle));
    }

    @Test
    void testGetAvailableStock() {
        ArrayList<Entry<Bicycle, Integer>> map = bookingBean.getAvailableStock();
        assertEquals(map, bookingBean.getAvailableStock());
    }

    @Test
    void testMakeBooking() {
        bookingBean.makeBooking();
    }

    @Test
    void testGetBicycleCountMap() {
//        fail("Not yet implemented");
    }

    @Test
    void testSetBicycleCountMap() {
//        fail("Not yet implemented");
    }

    @Test
    void testGetCartBean() {
//        fail("Not yet implemented");
    }

    @Test
    void testSetCartBean() {
//        fail("Not yet implemented");
    }

    @Test
    void testGetBookingDao() {
//        fail("Not yet implemented");
    }

    @Test
    void testSetBookingDao() {
//        fail("Not yet implemented");
    }

    @Test
    void testGetSecurityBean() {
//        fail("Not yet implemented");
    }

    @Test
    void testSetSecurityBean() {
//        fail("Not yet implemented");
    }

}
