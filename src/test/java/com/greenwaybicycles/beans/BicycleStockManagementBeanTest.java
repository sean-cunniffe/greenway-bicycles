package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.dao.BicycleStockManagementDao;
import com.greenwaybicycles.dao.BookingDao;
import com.greenwaybicycles.entity.Bicycle;
import com.greenwaybicycles.entity.Booking;
import com.greenwaybicycles.entity.User;

class BicycleStockManagementBeanTest {

    BicycleStockManagementBean bsmb;

    @BeforeEach
    void setUp() throws Exception {
        bsmb = new BicycleStockManagementBean();
        BicycleStockManagementDao stockManagement = new BicycleStockManagementDao();
        stockManagement.setUniqueBicycles(new ArrayList<Bicycle>());
        // our init method populates the locations with bicycles, for testing we dont
        // want these bicycles in it
        HashMap<String, ArrayList<Bicycle>> locations = new HashMap<String, ArrayList<Bicycle>>();
        stockManagement.setLocations(locations);
        locations.put("Mullingar", new ArrayList<Bicycle>());
        locations.put("Athlone", new ArrayList<Bicycle>());
        locations.put("Castletown", new ArrayList<Bicycle>());
        bsmb.setBicycleStockManagementDao(stockManagement);
        bsmb.setSecurityBean(new SecurityBean());
        bsmb.setBookingDao(new BookingDao());
        bsmb.init();
    }

    @Test
    void testGetEditBicycle() {
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.SIZE_MEDIUM);
        bsmb.setEditBicycle(bicycle);
        assertEquals(bicycle, bsmb.getEditBicycle());
    }

    @Test
    void testSetEditBicycle() {
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.SIZE_MEDIUM);
        bsmb.setEditBicycle(bicycle);
        assertEquals(bicycle, bsmb.getEditBicycle());
    }

    @Test
    void testAddBicycle() {
        bsmb.setEditStockLocation("Athlone");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.SIZE_MEDIUM);
        bsmb.addBicycle(bicycle);
        assertEquals(1, bsmb.getBicycleStockManagementDao().getBicyclesFromLocation("Athlone").size());
    }

    @Test
    void testRemoveBicycle() {
        bsmb.setEditStockLocation("Athlone");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.SIZE_MEDIUM);
        bsmb.addBicycle(bicycle);
        assertEquals(1, bsmb.getBicycleStockManagementDao().getBicyclesFromLocation("Athlone").size());
        bsmb.removeBicycle(bicycle);
        assertEquals(0, bsmb.getBicycleStockManagementDao().getBicyclesFromLocation("Athlone").size());
    }

    @Test
    void testLocations() {
        assertEquals(3, bsmb.locations().size());
    }

    @Test
    void testGetStockCount() {
        bsmb.setEditStockLocation("Athlone");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.SIZE_MEDIUM);
        bsmb.addBicycle(bicycle);
        assertEquals(1, bsmb.getStockCount("Athlone").get(0).getValue());
    }

    @Test
    void testGetBicyclesNeededForBookings() {
        bsmb.setEditStockLocation("Castletown");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT);
        bsmb.addBicycle(bicycle);
        Booking booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Mullingar");
        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT));
        bsmb.getBookingDao().addBooking(booking);
        assertEquals(1,bsmb.getBicyclesNeededForBookings("Castletown").get(0).getValue());
    }

    @Test
    void testGetIncomingStockFromBookings() {
        bsmb.setEditStockLocation("Castletown");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT);
        bsmb.addBicycle(bicycle);
        Booking booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
                LocalDate.now().plusDays(0), LocalDate.now().plusDays(1), "Castletown", "Castletown");
        booking.setPickedUp(true);
        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT));
        bsmb.getBookingDao().addBooking(booking);
        assertEquals(1,bsmb.getIncomingStockFromBookings("Castletown").get(0).getValue());
    }

    @Test
    void testGetStockBalancesAfterBookings() {
        bsmb.setEditStockLocation("Castletown");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT);
        bsmb.addBicycle(bicycle);
        Booking booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
                LocalDate.now().plusDays(0), LocalDate.now().plusDays(1), "Castletown", "Castletown");
        booking.setPickedUp(true);
        booking.getBicycles().add(new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT));
        bsmb.getBookingDao().addBooking(booking);
        assertEquals(1,bsmb.getStockBalancesAfterBookings("Castletown").get(0).getValue());
    }

    @Test
    void testChangeBicyclePrices() {
        Booking booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
                LocalDate.now().plusDays(0), LocalDate.now().plusDays(1), "Castletown", "Castletown");
        bsmb.setEditStockLocation("Castletown");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT);
        booking.getBicycles().add(bicycle);
        bsmb.getBookingDao().addBooking(booking);
        bsmb.addBicycle(bicycle);
        Bicycle bicycle2 = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 50, Bicycle.MODEL_GIANT);
        bsmb.changeBicyclePrices(bicycle2);
        assertEquals(50, bsmb.getBicycleStockManagementDao().getBicyclesFromLocation("Castletown").get(0).getPricePerDay());
    }
    
    @Test
    void testMoveStock() throws Exception {
        bsmb.setFromLocation("Athlone");
        bsmb.setToLocation("Castletown");
        bsmb.setEditStockLocation("Athlone");
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_EXTRA_LARGE, 20, Bicycle.MODEL_GIANT);
        bsmb.addBicycle(bicycle);
        bsmb.moveStock(bicycle);
        assertEquals(1, bsmb.getBicycleStockManagementDao().getBicyclesFromLocation("Castletown").size());
        
    }

}