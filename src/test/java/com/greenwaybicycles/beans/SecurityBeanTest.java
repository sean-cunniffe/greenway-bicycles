package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Spy;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;

class SecurityBeanTest {

    SecurityBean securityBean;

    User user;

    @BeforeEach
    void setUp() throws Exception {

        // set up spy
        SecurityBean spyBean = new SecurityBean();
        securityBean = spy(spyBean);
        // mock methods on securityBean to avoid FaceContext
        HashMap<String, Object> mockingMap = new HashMap<String, Object>();
        mockingMap.put("loginBean", null);
        mockingMap.put("DashboardBean", null);
        Mockito.doReturn(mockingMap).when(securityBean).getSessionBeans();
        Mockito.doReturn(true).when(securityBean).redirect(Mockito.anyString());
        Mockito.doNothing().when(securityBean).showMessage(Mockito.any(), Mockito.anyString(), Mockito.any(),
                Mockito.anyString());
        UserDAO userDao = new UserDAO();
        user = userDao.addUser("sean", "cunniffe", "Test123", "sean.cunniffe927@gmail.com", "0871234567", "Someplace",
                User.ROLE_STAFF, "Dogs name?", "Ted");
        securityBean.setUserDao(userDao);
        securityBean.setUser(user);
    }

    @Test
    void testCheckRole() throws Exception {
        assertTrue(securityBean.checkRole(User.ROLE_STAFF));
        securityBean.setUser(null);
        assertFalse(securityBean.checkRole(User.ROLE_CUSTOMER));
        assertFalse(securityBean.checkRole(User.ROLE_CUSTOMER, User.ROLE_STAFF));

    }

    @Test
    void verifyUserCredSuccessful() throws Exception {
        assertTrue(securityBean.verifyUserCred("sean.cunniffe927@gmail.com", "Test123"));
    }

    @Test
    void verifyUserCredWrongEmail() throws Exception {
        assertFalse(securityBean.verifyUserCred("sean.nniffe927@gmail.com", "Test123"));
    }

    @Test
    void verifyUserCredWrongPassword() throws Exception {
        assertFalse(securityBean.verifyUserCred("sean.cunniffe927@gmail.com", "Test1"));
    }

    @Test
    void verifyUserCredWrongEmailAndPassword() throws Exception {
        assertFalse(securityBean.verifyUserCred("secunniffe927@gmail.com", "t123"));
    }

    @Test
    void testCheckQuestion() throws Exception {
        securityBean.checkQuestion("Ted", "sean.cunniffe927@gmail.com");
        securityBean.checkQuestion("Te", "sean.cunniffe927@gmail.com");
    }

    @Test
    void testGetSecurityQuestionFromEmail() throws Exception {
        securityBean.getSecurityQuestionFromEmail("sean.cunniffe927@gmail.com");
        assertEquals("Dogs name?", securityBean.getSecurityQuestion());
        securityBean.getSecurityQuestionFromEmail("se@gmail.com");
        assertEquals("Dogs name?", securityBean.getSecurityQuestion());
    }

    @Test
    void testVerifyPassword() throws Exception {
        assertEquals(SecurityBean.CHANGE_PASSWORD, securityBean.verifyPassword("Test123"));
        assertEquals(SecurityBean.MANAGE_ACCOUNT, 
                securityBean.verifyPassword("Test12"));
    }
    
    @Test
    void testChangePassword() throws Exception {
        securityBean.setChangePassword(true);
        assertDoesNotThrow(()->{securityBean.changePassword("Test1234");});
        securityBean.setChangePassword(false);
        assertDoesNotThrow(()->{securityBean.changePassword("Test1234");});
    }

    @Test
    void testLogout() throws Exception {
        securityBean.logout();
    }

}
