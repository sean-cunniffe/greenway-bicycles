package com.greenwaybicycles.beans;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.dao.UserDAO;
import com.greenwaybicycles.entity.User;

class LoginBeanTest {
    private LoginBean testLoginBean;
    private SecurityBean securityBean;
    
    @BeforeEach
    public void setUp() {
        testLoginBean = new LoginBean();
        securityBean = new SecurityBean();
        testLoginBean.setSecurityBean(securityBean); 
        UserDAO userDao = new UserDAO();
        userDao.addUser("john", "doe", "Test123", "john.doe@gmail.com", "0872345678", "someplace", User.ROLE_STAFF);
        securityBean.setUserDao(userDao);
    }

    @Test
    void testChangeEmail() {
        testLoginBean.setEmail("dave.doe@gmail.com");
        assertEquals("dave.doe@gmail.com", testLoginBean.getEmail());
    }

    @Test
    void testChangePassword() {
        testLoginBean.setPassword("passtest");
        assertEquals("passtest", testLoginBean.getPassword());
    }

	@Test
	void testValidateUserLogin() {
		testLoginBean.setEmail("john.doe@gmail.com");
		testLoginBean.setPassword("Test123");
		assertEquals("dashboard", testLoginBean.validateUserLogin());
	}

    @Test
    void testValidateWrongUserLogin() {
        testLoginBean.setEmail("mick.doe@gmail.com");
        testLoginBean.setPassword("Test124");
        assertEquals("login", testLoginBean.validateUserLogin());
    }
    
    @Test
    void testValidateUserLoginWithRememberMe() {
        testLoginBean.setEmail("john.doe@gmail.com");
        testLoginBean.setPassword("Test123");
        testLoginBean.setRememberLogin(true);
        assertEquals("dashboard", testLoginBean.validateUserLogin());
        assertNotNull(testLoginBean.getEmail());
        assertNotNull(testLoginBean.getPassword());
    }
    
    @Test
    void testValidateUserLoginWithoutRememberMe() {
        testLoginBean.setEmail("john.doe@gmail.com");
        testLoginBean.setPassword("Test123");
        testLoginBean.setRememberLogin(false);
        assertEquals("dashboard", testLoginBean.validateUserLogin());
        assertNull(testLoginBean.getEmail());
        assertNull(testLoginBean.getPassword());
    }

    @Test
    void testChangeRememberLogin() {
        testLoginBean.setRememberLogin(false);
        assertFalse(testLoginBean.isRememberLogin());
    }
    
    
}
