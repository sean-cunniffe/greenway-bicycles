package com.greenwaybicycles.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.entity.User;

class UserDAOTest {

    UserDAO userDao;
    User user;

    @BeforeEach
    void setUp() throws Exception {
        userDao = new UserDAO();
        userDao.init();
        userDao.setUsers(new ArrayList<User>());
        userDao.setStaffEmails(new ArrayList<String>());

        user = userDao.addUser("John", "Doe", "Test123", "John.doe@gmail.com", "09876546778", "Athlone",
                User.ROLE_CUSTOMER);
        userDao.addStaffEmail("sc@gmail.com");
    }

    @Test
    void testAddUser() {
        assertEquals(user, userDao.getUsers().get(0));
    }

    @Test
    void testAddUserWithUserEmail() {
        assertNotNull(
                userDao.addUser("Sean", "Doe", "password", "sean@gmail.com", "0871234567", "someplace", "Manager"));
        assertNull(userDao.addUser("Sean", "Doe", "password", "sean@gmail.com", "0871234567", "someplace", "Manager"));
    }

    @Test
    void testGetUserByEmail() {
        assertEquals(user, userDao.getUserByEmail("john.doe@gmail.com"));
    }

    @Test
    void testUpdateUserByEmailUpdateFirstName() {
        userDao.updateUserByEmail("Kevin", null, "Test123", "john.doe@gmail.com");
        assertEquals("Kevin", user.getFirstName());
    }

    @Test
    void testUpdateUserByEmailUpdateSurname() {
        userDao.updateUserByEmail(null, "John", "Test123", "john.doe@gmail.com");
        assertEquals("John", user.getSurname());
    }

    @Test
    void testDeleteUserByEmailSuccessful() {
        userDao.deleteUserByEmail("john.doe@gmail.com");
        assertEquals(0, userDao.getUsers().size());
    }

    @Test
    void testDeleteUserByEmailUnSuccessful() {
        userDao.deleteUserByEmail("sean.doe@gmail.com");
        assertEquals(1, userDao.getUsers().size());
    }

    @Test
    void testAddUserWithInvalidRole() {
        User user = userDao.addUser("Sean", "Doe", "password", "sean@gmail.com", "0871234567", "someplace", "Manager");
        assertEquals(User.ROLE_CUSTOMER, user.getRole());
    }

    @Test
    void testAddUserWithvalidRole() {
        User user = userDao.addUser("Sean", "Doe", "password", "sean@gmail.com", "0871234567", "someplace",
                User.ROLE_STAFF);
        assertEquals(User.ROLE_STAFF, user.getRole());
    }

    @Test
    void addExistingStaffEmail() {
        assertFalse(userDao.addStaffEmail("sc@gmail.com"));
    }

    @Test
    void testGetStaff() {
        user = userDao.addUser("Sean", "Doe", "Test123", "sean@gmail.com", "09876546778", "Athlone", User.ROLE_STAFF);
        assertNotNull(user);
        assertEquals(1, userDao.getStaff().size());
    }

    @Test
    void testGetCustomers() throws Exception {
        assertEquals(1, userDao.getCustomer().size());
    }

    @Test
    void testDeleteUserByEmail() throws Exception {
        user = userDao.addUser("Sean", "Doe", "Test123", "sean@gmail.com", "09876546778", "Athlone", User.ROLE_STAFF);
        userDao.deleteUserByEmail(user.getEmail());
        assertEquals(null,userDao.getUserByEmail("sean@gmail.com"));
    }
}
