package com.greenwaybicycles.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.entity.Bicycle;

class BicycleStockManagementDaoTest {

    BicycleStockManagementDao bsm;

    @BeforeEach
    void setUp() {
        bsm = new BicycleStockManagementDao();
        bsm.init();
        bsm.setUniqueBicycles(new ArrayList<Bicycle>());
        // our init method populates the locations with bicycles, for testing we dont
        // want these bicycles in it
        HashMap<String, ArrayList<Bicycle>> locations = new HashMap<String, ArrayList<Bicycle>>();
        bsm.setLocations(locations);
        locations.put("Mullingar", new ArrayList<Bicycle>());
        locations.put("Athlone", new ArrayList<Bicycle>());
        locations.put("Castletown", new ArrayList<Bicycle>());
    }

    @Test
    void testAddBicycle() {
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 30));
        assertEquals(1, bsm.getBicyclesFromLocation("Mullingar").size());
    }

    @Test
    void testGetLocations() {
        ArrayList<String> locations = bsm.getLocations();
        assertEquals(3, locations.size());
        assertTrue(locations.contains("Mullingar"));
        assertTrue(locations.contains("Athlone"));
        assertTrue(locations.contains("Castletown"));
    }

    @Test
    void testMoveBicycleSuccessful() {
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.moveBicycle("Mullingar", "Athlone", Bicycle.SIZE_SMALL));
        assertEquals(2, bsm.getBicyclesFromLocation("Mullingar").size());
        assertEquals(1, bsm.getBicyclesFromLocation("Athlone").size());
    }

    @Test
    void testMoveBicycleWithNoBicycleInFromLocation() {
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 30));
        assertFalse(bsm.moveBicycle("Mullingar", "Athlone", Bicycle.SIZE_SMALL));
        assertEquals(3, bsm.getBicyclesFromLocation("Mullingar").size());
        assertEquals(0, bsm.getBicyclesFromLocation("Athlone").size());
    }

    @Test
    void testMoveBicycle() {
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_SMALL, 30, Bicycle.MODEL_GIANT);
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.moveBicycle("Mullingar", "Athlone", bicycle));
        assertEquals(2, bsm.getBicyclesFromLocation("Mullingar").size());
        assertEquals(1, bsm.getBicyclesFromLocation("Athlone").size());
    }

    @Test
    void testChangeBicyclePrice() {
        Bicycle bicycle = new Bicycle(Bicycle.SIZE_SMALL, 50, Bicycle.MODEL_GIANT);
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        bsm.changeBicyclePrices(bicycle);
        assertEquals(50, bsm.getAllBicycles().get(0).getPricePerDay());
    }

    @Test
    /**
     * Bicycle has overridden equals method, meaning the equals method looks at the
     * attributes rather than the memory allocation
     */
    void testGetUniqueBicycles() {

        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_SMALL, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_MEDIUM, Bicycle.MODEL_GIANT, 30));
        assertTrue(bsm.addBicycle("Mullingar", Bicycle.SIZE_LARGE, Bicycle.MODEL_GIANT, 30));

        assertEquals(3, bsm.getUniqueBicycles().size());
        Bicycle b1 = new Bicycle(Bicycle.SIZE_SMALL, 30, Bicycle.MODEL_GIANT);
        Bicycle b2 = new Bicycle(Bicycle.SIZE_MEDIUM, 30, Bicycle.MODEL_GIANT);
        Bicycle b3 = new Bicycle(Bicycle.SIZE_LARGE, 30, Bicycle.MODEL_GIANT);

        assertTrue(bsm.getUniqueBicycles().contains(b1));
        assertTrue(bsm.getUniqueBicycles().contains(b2));
        assertTrue(bsm.getUniqueBicycles().contains(b3));
    }

    @Test
    void testAddListOfBicycles() throws Exception {
        Bicycle b1 = new Bicycle(Bicycle.SIZE_SMALL, 30, Bicycle.MODEL_GIANT);
        Bicycle b2 = new Bicycle(Bicycle.SIZE_MEDIUM, 30, Bicycle.MODEL_GIANT);
        Bicycle b3 = new Bicycle(Bicycle.SIZE_LARGE, 30, Bicycle.MODEL_GIANT);
        ArrayList<Bicycle> bicycles = new ArrayList<>();
        bicycles.add(b1);
        bicycles.add(b2);
        bicycles.add(b3);
        bsm.addBicycles(bicycles, "Mullingar");
        assertEquals(3, bsm.getBicyclesFromLocation("Mullingar").size());
    }

    @Test
    void testRemoveBicycle() throws Exception {
        Bicycle b1 = new Bicycle(Bicycle.SIZE_SMALL, 30, Bicycle.MODEL_GIANT);
        Bicycle b2 = new Bicycle(Bicycle.SIZE_MEDIUM, 30, Bicycle.MODEL_GIANT);
        Bicycle b3 = new Bicycle(Bicycle.SIZE_LARGE, 30, Bicycle.MODEL_GIANT);
        // bsm.addBicycles(List.of(b1, b2, b3), "Mullingar");
        ArrayList<Bicycle> bicycles = new ArrayList<>();
        bicycles.add(b1);
        bicycles.add(b2);
        bicycles.add(b3);
        bsm.addBicycles(bicycles, "Mullingar");
        assertEquals(3, bsm.getBicyclesFromLocation("Mullingar").size());
        bsm.removeBicycle("Mullingar", b1);
        assertEquals(2, bsm.getBicyclesFromLocation("Mullingar").size());
    }

}
