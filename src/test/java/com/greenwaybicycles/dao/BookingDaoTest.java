package com.greenwaybicycles.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.greenwaybicycles.entity.Booking;
import com.greenwaybicycles.entity.User;

class BookingDaoTest {

	BookingDao bookingDao;
	Booking booking;

	@BeforeEach
	void setUp() {
		bookingDao = new BookingDao();
		bookingDao.init();
		Booking booking = new Booking(new User("Sean", "Cunniffe", "", "sean@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Mullingar");
		bookingDao.addBooking(booking);
	}


	@Test
	void testAddBooking() {
		Booking bookingTest = new Booking(new User("David", "Finlay", "", "df@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Castletown");
		bookingDao.addBooking(bookingTest);
		assertEquals(2,bookingDao.getAllBookings().size());
	}
	

	@Test
	void testGetBookingsByUsersEmail() {
		Booking bookingTest = new Booking(new User("David", "Finlay", "", "df@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Castletown", "Athlone");
		bookingDao.addBooking(bookingTest);
		assertEquals(1,bookingDao.getBookingsByUsersEmail("df@gmail.com").size());
		}

	@Test
	void testGetBookingsFromLocation() {
		Booking bookingTest1 = new Booking(new User("David", "Finlay", "", "df@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Athlone", "Athlone");
		bookingDao.addBooking(bookingTest1);
		Booking bookingTest2 = new Booking(new User("David", "Finlay", "", "df@gmail.com", "0871234567", "someplace"),
                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Mullingar", "Castletown");
        bookingDao.addBooking(bookingTest2);
        assertEquals(1,bookingDao.getBookingsFromLocation("Athlone").size());
		assertEquals(2,bookingDao.getBookingsFromLocation("Castletown").size());
		
		
	}

	@Test
	void testGetActiveBookingsFromLocation() {
		Booking bookingTest = new Booking(new User("David", "Finlay", "", "df@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Athlone", "Athlone");
		bookingDao.addBooking(bookingTest);
		assertEquals(1,bookingDao.getActiveBookingsFromLocation("Athlone").size());
	}

	@Test
	void testRemoveBooking() {
		Booking bookingTest = new Booking(new User("David", "Finlay", "", "df@gmail.com", "0871234567", "someplace"),
				LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), "Athlone", "Athlone");
		bookingDao.addBooking(bookingTest);
		assertEquals(1,bookingDao.getActiveBookingsFromLocation("Athlone").size());
		bookingDao.removeBooking(bookingTest);
		assertEquals(0,bookingDao.getActiveBookingsFromLocation("Athlone").size());
	}

	@Test
	void testGetBicyclesFromBookingsInLocationOnDate() {
		assertEquals(0, bookingDao.getBicyclesFromBookingsInLocationOnDate("Athlone", LocalDate.now().plusDays(1)).size());
	}

	@Test
	void testGetBicyclesPickups() {
		assertEquals(0,bookingDao.getBicyclesPickups("Athlone", LocalDate.now().plusDays(1)));
	}

	@Test
	void testGetBicyclesDropOffs() {
		assertEquals(0,bookingDao.getBicyclesDropOffs("Athlone", LocalDate.now().plusDays(1)));

	}

}
