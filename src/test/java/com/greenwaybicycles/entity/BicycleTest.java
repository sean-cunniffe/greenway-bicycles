package com.greenwaybicycles.entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BicycleTest {

    Bicycle bicycle;

    @BeforeEach
    void setUp() throws Exception {
        bicycle = new Bicycle(Bicycle.SIZE_MEDIUM, 20, Bicycle.MODEL_GIANT);
    }

    @Test
    void testConstructor() {
        assertEquals("Medium", bicycle.getSize());
        assertEquals(20, bicycle.getPricePerDay());
        assertTrue(bicycle.toString() instanceof String);
        bicycle.setPricePerDay(30);
        bicycle.setSize(Bicycle.SIZE_SMALL);
        assertEquals("Small", bicycle.getSize());
        assertEquals(30, bicycle.getPricePerDay());
        bicycle.setSortingValue(1);
        assertEquals(1, bicycle.getSortingValue());
        bicycle.setModel("Giant");
        assertEquals("Giant", bicycle.getModel());
    }
    
    @Test
    void testEqualsTrue() {
        Bicycle bicycle1 = new Bicycle(Bicycle.SIZE_SMALL, 20, Bicycle.MODEL_GIANT);
        Bicycle bicycle2 = new Bicycle(Bicycle.SIZE_SMALL, 20.0, Bicycle.MODEL_GIANT);
        assertTrue(bicycle1.equals(bicycle2));
    }
    
    @Test
    void testEqualsFalse() {
        Bicycle bicycle1 = new Bicycle(Bicycle.SIZE_SMALL, 20, Bicycle.MODEL_GIANT);
        Bicycle bicycle2 = new Bicycle(Bicycle.SIZE_MEDIUM, 20.0, Bicycle.MODEL_GIANT);
        assertFalse(bicycle1.equals(bicycle2));
    }

}
