package com.greenwaybicycles.entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OrderDetailTest {

    OrderDetail orderDetail;

    @BeforeEach
    void setUp() throws Exception {
        orderDetail = new OrderDetail("Giant", 20.0f, 20.0f);
    }

    @Test
    void testConstructor() {
        assertEquals("Giant", orderDetail.getProductName());
        assertEquals("20.00", orderDetail.getSubtotal());
        assertEquals("20.00", orderDetail.getTotal());
        assertTrue(orderDetail.toString() instanceof String);

    }

}
