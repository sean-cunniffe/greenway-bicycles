package com.greenwaybicycles.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BookingTest {
	
	 private User myUser;
	
	Booking booking;

	@BeforeEach
    void setUp() throws Exception {
		
		myUser = new User("James", "Foley", "testPassword", "jamesfoley@gmail.com", "087-1234567", "Athlone");
        booking = new Booking(myUser, LocalDate.parse("2021-08-10"), LocalDate.parse("2021-08-11"), "Athlone", "Mullingar");
    }

	@Test
    void testConstructor() {
        assertEquals("Athlone", booking.getPickupLocation());
        assertEquals("Mullingar", booking.getDropOffLocation());
        assertEquals(LocalDate.parse("2021-08-11"), booking.getEndDate());
        assertEquals(LocalDate.parse("2021-08-10"), booking.getStartDate());
        booking.setStartDate(LocalDate.parse("2021-08-01"));
        assertEquals(LocalDate.parse("2021-08-01"), booking.getStartDate());
        booking.setEndDate(LocalDate.parse("2021-08-04"));
        assertEquals(LocalDate.parse("2021-08-04"), booking.getEndDate());
        booking.setPickedUp(true);
        assertTrue(booking.isPickedUp());
        assertTrue(booking.toString() instanceof String);
        booking.setDropOffLocation("Castletown");
        booking.setPickupLocation("Castletown");
        assertEquals("Castletown", booking.getDropOffLocation());
        assertEquals("Castletown", booking.getPickupLocation());
       
    }
	
    @Test
    public void testChangePickUpLocation() {

        booking.setPickupLocation("Athlone");
        assertEquals("Athlone", booking.getPickupLocation());

    }

    @Test
    public void testChangeDropOffLocation() {

        booking.setDropOffLocation("Castletown");
        assertEquals("Castletown", booking.getDropOffLocation());

    }

}
