package com.greenwaybicycles.entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCrypt.Hasher;
import at.favre.lib.crypto.bcrypt.BCrypt.Verifyer;
import at.favre.lib.crypto.bcrypt.BCrypt.Version;

class UserTest {

    private final Version HASHER_VERSION = Version.VERSION_2A;
    private User myUser;
    private Hasher hasher = BCrypt.with(HASHER_VERSION);
    private Verifyer verifyer = BCrypt.verifyer(HASHER_VERSION);
    private String testPassword = "$2a$10$VyOP4DCxZf9Y.lfhn0FOluB7AGvIgJC6pm8.mcNpPBsotgwQe6OMy";

    @BeforeEach
    public void setUp() {
        //password: Test123
        myUser = new User("James", "Foley", testPassword, "jamesfoley@gmail.com", "087-1234567", "Athlone");
    }

    @Test
    public void testUserConstructor() {
        assertEquals("James", myUser.getFirstName());
        assertEquals("Foley", myUser.getSurname());
        assertEquals("$2a$10$VyOP4DCxZf9Y.lfhn0FOluB7AGvIgJC6pm8.mcNpPBsotgwQe6OMy", myUser.getPassword());
        assertEquals("jamesfoley@gmail.com", myUser.getEmail());

    }

    @Test
    public void testChangeUserFirstName() {

        myUser.setFirstName("Tom");
        assertEquals("Tom", myUser.getFirstName());

    }

    @Test
    public void testChangeUserLastName() {

        myUser.setSurname("Farrell");
        assertEquals("Farrell", myUser.getSurname());

    }

    @Test
    public void testChangePasswordSuccessful() {
        String passwordHashed = hasher.hashToString(4, "00000".toCharArray());
        myUser.setPassword(passwordHashed);
        assertTrue(verifyer.verify("00000".toCharArray(), myUser.getPassword()).verified);
    }

    @Test
    public void testChangePasswordFailed() {
        // password not hashed so doesnt set the password
        myUser.setPassword("password123");
        assertNotEquals("password", myUser.getPassword());

    }

    @Test
    public void testChangeEmail() {
        myUser.setEmail("tomfarrell@gmail.com");
        assertEquals("tomfarrell@gmail.com", myUser.getEmail());

    }

    @Test
    public void testChangePhone() {
        myUser.setPhone("086-1234567");
        assertEquals("086-1234567", myUser.getPhone());

    }

    @Test
    public void testChangeAddress() {
        myUser.setAddress("Moate");
        assertEquals("Moate", myUser.getAddress());

    }
    
    @Test
    public void testCreateStaffWithRoleConstructor() {
        User user = new User("Sean", "doe", testPassword, "sean@gmail.com", "0871234567", "someplace", User.ROLE_STAFF);
        assertEquals(User.ROLE_STAFF, user.getRole());
    }
    
    @Test
    public void testCreateCustomerWithRoleConstructor() {
        User user = new User("Sean", "doe", testPassword, "sean@gmail.com", "0871234567", "someplace", User.ROLE_CUSTOMER);
        assertEquals(User.ROLE_CUSTOMER, user.getRole());
    }
    
    @Test
    public void testCreateUserWithInvalidRoleConstructor() {
        User user = new User("Sean", "doe", testPassword, "sean@gmail.com", "0871234567", "someplace", "Manager");
        assertNotEquals("Manager", user.getRole());
        assertEquals(User.ROLE_CUSTOMER, user.getRole());
    }
    
    @Test
    void testSetRoleStaff() {
        myUser.setRole(User.ROLE_STAFF);
        assertEquals(User.ROLE_STAFF, myUser.getRole());
    }
    
    @Test
    void testSetRoleCustomer() {
        myUser.setRole(User.ROLE_STAFF);
        assertEquals(User.ROLE_STAFF, myUser.getRole());
    }
    
    @Test
    public void testChangeSecurityQuestion() {
        myUser.setSecurityQuestion("Test Question");;
        assertEquals("Test Question", myUser.getSecurityQuestion());
    }
    
    @Test
    public void testChangeSecurityAnswer() {
        myUser.setSecurityAnswer("Test Answer");;
        assertEquals("Test Answer", myUser.getSecurityAnswer());
    }

}
